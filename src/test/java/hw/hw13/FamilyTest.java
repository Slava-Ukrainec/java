package hw.hw13;

import hw.hw13.entities.Family;
import hw.hw13.entities.people.Human;
import hw.hw13.entities.people.Man;
import hw.hw13.entities.people.Woman;
import hw.hw13.entities.pets.DomesticCat;
import hw.hw13.entities.pets.Pet;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {

    private Family testFamily;
    private Human mother;
    private Human father;
    static Pet cat;

    @BeforeAll
    static void beforeAll(){
        cat = new DomesticCat("Dora", 1, 75, "Plays", "Sleeps", "Messes up with dogs");
    }

    @BeforeEach
    void beforeEach() {
        mother = new Woman("Zoya", "Mendel", "05/03/1915");
        father = new Man("Simon", "Mendel", "21/08/1910");
        testFamily = new Family(father,mother);
    }

    @Test
    void initiation_basic(){
        assertEquals(mother,testFamily.getMother());
        assertEquals(testFamily,father.getFamily());
        assertEquals(father,testFamily.getFather());
        assertEquals(testFamily, father.getFamily());
        assertEquals(0, testFamily.getChildren().size());
        assertEquals(0, testFamily.getAllPets().size());
    }

    @Test
    void initiation_with_children(){
        Human claraS = new Woman("Klara", "Stevenson", "08/05/1965");
        Human adamM = new Man("Adam", "Mendel", "12/07/1965", 75);
        Family testFamily = new Family(father,mother, new Human[]{claraS,adamM});
        assertEquals(mother,testFamily.getMother());
        assertEquals(testFamily,mother.getFamily());
        assertEquals(father,testFamily.getFather());
        assertEquals(testFamily, father.getFamily());
        assertEquals(2, testFamily.getChildren().size());
        childMustBelongToFamily(testFamily, claraS);
        childMustBelongToFamily(testFamily, adamM);
        assertEquals(0, testFamily.getAllPets().size());
    }

    @Test
    void initiation_with_pet(){
        Family testFamily = new Family(father,mother, cat);
        assertEquals(mother,testFamily.getMother());
        assertEquals(testFamily,mother.getFamily());
        assertEquals(father,testFamily.getFather());
        assertEquals(testFamily, father.getFamily());
        assertEquals(0, testFamily.getChildren().size());
        assertTrue(testFamily.getAllPets().contains(cat));
    }

    @Test
    void initiation_with_children_and_pet(){
        Human claraS = new Woman("Klara", "Stevenson", "08/05/1965");
        Human adamM = new Man("Adam", "Mendel", "12/07/1965", 75);
        Family testFamily = new Family(father,mother, new Human[]{claraS,adamM}, cat);
        assertEquals(mother,testFamily.getMother());
        assertEquals(testFamily,mother.getFamily());
        assertEquals(father,testFamily.getFather());
        assertEquals(testFamily, father.getFamily());
        assertEquals(2, testFamily.getChildren().size());
        childMustBelongToFamily(testFamily, claraS);
        childMustBelongToFamily(testFamily, adamM);
        assertTrue(testFamily.getAllPets().contains(cat));
    }

    @Test
    void addChildValid() {
        Human claraS = new Woman("Klara", "Stevenson", "08/05/1965");
        List<Human> children = testFamily.addChild(claraS);
        assertEquals(1, testFamily.getChildren().size());
        assertEquals(testFamily, claraS.getFamily());
        assertEquals(children, testFamily.getChildren());
        childMustBelongToFamily(testFamily, claraS);
    }

    @Test
    void addChildTwice() {
        Human claraS = new Woman("Klara", "Stevenson", "08/05/1965");
        List<Human> children1 = testFamily.addChild(claraS);
        List<Human> children2 = testFamily.addChild(claraS);
        assertEquals(1, testFamily.getChildren().size());
        assertEquals(children1, children2);
        childMustBelongToFamily(testFamily, claraS);
    }

    @Test
    void addChild_parents_as_children() {
        testFamily.addChild(father);
        assertEquals(0, testFamily.getChildren().size());
        List<Human> children = testFamily.addChild(mother);
        assertEquals(0, testFamily.getChildren().size());
        assertEquals(0, children.size());
    }

    @Test
    void addChildrenValid() {
        Human claraS = new Woman("Klara", "Stevenson", "08/05/1965");
        Human adamM = new Man("Adam", "Mendel", "12/07/1965", 75);
        List<Human> children = testFamily.addChildren(new Human[]{claraS, adamM});
        assertEquals(2, testFamily.getChildren().size());
        childMustBelongToFamily(testFamily, claraS);
        childMustBelongToFamily(testFamily, adamM);
        assertEquals(2, children.size());
        assertTrue(children.contains(claraS));
        assertTrue(children.contains(adamM));
    }

    @Test
    void add_Children_Valid_as_ArrayList() {
        Human claraS = new Woman("Klara", "Stevenson", "08/05/1965");
        Human adamM = new Man("Adam", "Mendel", "12/07/1965", 75);
        List<Human> children = testFamily.addChildren(Arrays.asList(claraS, adamM));
        assertEquals(2, testFamily.getChildren().size());
        childMustBelongToFamily(testFamily, claraS);
        childMustBelongToFamily(testFamily, adamM);
        assertEquals(2, children.size());
        assertTrue(children.contains(claraS));
        assertTrue(children.contains(adamM));
    }

    @Test
    void setPetValid() {
        testFamily.addPet(cat);
        assertTrue(testFamily.getAllPets().contains(cat));
    }

    @Test
    void deleteChild_Child_entry_Valid() {
        Human claraS = new Woman("Klara", "Stevenson", "08/05/1965");
        Human adamM = new Man("Adam", "Mendel", "12/07/1965", 75);
        testFamily.addChildren(new Human[]{claraS,adamM});
        boolean result = testFamily.deleteChild(adamM);
        assertEquals(1, testFamily.getChildren().size());
        childMustBelongToFamily(testFamily, claraS);
        assertNull(adamM.getFamily());
        assertTrue(result);
    }

    @Test
    void deleteChild_Child_entry_non_child() {
        Human claraS = new Woman("Klara", "Stevenson", "08/05/1965");
        Human adamM = new Man("Adam", "Mendel", "12/07/1965", 75);
        testFamily.addChildren(new Human[]{claraS});
        boolean result = testFamily.deleteChild(adamM);
        assertEquals(1, testFamily.getChildren().size());
        childMustBelongToFamily(testFamily, claraS);
        assertFalse(result);
    }

    @Test
    void deleteChild_index_entry_Valid() {
        Human claraS = new Woman("Klara", "Stevenson", "08/05/1965");
        Human adamM = new Man("Adam", "Mendel", "12/07/1965", 75);
        testFamily.addChildren(new Human[]{claraS,adamM});
        boolean result = testFamily.deleteChild(1);
        assertEquals(1, testFamily.getChildren().size());
        childMustBelongToFamily(testFamily, claraS);
        assertNull(adamM.getFamily());
        assertTrue(result);
    }

    @Test
    void deleteChild_index_entry_invalid() {
        Human claraS = new Woman("Klara", "Stevenson", "08/05/1965");
        Human adamM = new Man("Adam", "Mendel", "12/07/1965", 75);
        testFamily.addChildren(new Human[]{claraS,adamM});
        boolean result = testFamily.deleteChild(2);
        assertEquals(2, testFamily.getChildren().size());
        childMustBelongToFamily(testFamily, claraS);
        childMustBelongToFamily(testFamily, adamM);
        assertFalse(result);
    }

    @Test
    void removePet(){
        testFamily.addPet(cat);
        assertTrue(testFamily.getAllPets().contains(cat));
        testFamily.removePet(cat);
        assertFalse(testFamily.getAllPets().contains(cat));
    }

    @Test
    void getFamilyCount(){
        assertEquals(2, testFamily.countFamily());
        Human claraS = new Woman("Klara", "Stevenson", "08/05/1965");
        Human adamM = new Man("Adam", "Mendel", "12/07/1965", 75);
        testFamily.addChildren(new Human[]{claraS,adamM});
        assertEquals(4,testFamily.countFamily());
        testFamily.addPet(cat);
        assertEquals(4,testFamily.countFamily());
    }

    @Test
    void equals_another_duplicate(){
        Family otherFamily = new Family(father, mother);
        assertEquals(testFamily, otherFamily);
    }

    @Test
    void equals_another_same(){
        Family otherFamily = testFamily;
        assertEquals(testFamily, otherFamily);
    }

    @Test
    void equals_another_null(){
        assertNotEquals(testFamily, null);
    }

    @Test
    void equals_another_difFather(){
        Human alexM = new Man("Alex", "Mendel", "21/08/1910");
        Family otherFamily = new Family(alexM, mother);
        assertNotEquals(testFamily, otherFamily);
    }

    @Test
    void equals_another_difMother(){
        Human zoyaM = new Woman("Anna", "Mendel", "05/03/1915");
        Family otherFamily = new Family(father, zoyaM);
        assertNotEquals(testFamily, otherFamily);
    }

    @Test
    void toStringBasic() {
        assertEquals("==========================================================================================================\n" +
                "MENDEL FAMILY\n" +
                "    father: {name = Simon, surname = Mendel, birthDate = 21/08/1910, iq = 0}\n" +
                "    mother: {name = Zoya, surname = Mendel, birthDate = 05/03/1915, iq = 0}", testFamily.toString());
    }

    @Test
    void toString_with_children() {
        Human claraS = new Woman("Klara", "Stevenson", "08/05/1965");
        Human adamM = new Man("Adam", "Mendel", "12/07/1965", 75);
        testFamily.addChildren(new Human[]{claraS,adamM});
        assertEquals("==========================================================================================================\n" +
                "MENDEL FAMILY\n" +
                "    father: {name = Simon, surname = Mendel, birthDate = 21/08/1910, iq = 0}\n" +
                "    mother: {name = Zoya, surname = Mendel, birthDate = 05/03/1915, iq = 0}\n" +
                "    children:\n" +
                "        woman: {name = Klara, surname = Stevenson, birthDate = 08/05/1965, iq = 0}\n" +
                "        man: {name = Adam, surname = Mendel, birthDate = 12/07/1965, iq = 75}", testFamily.toString());
    }

    @Test
    void toString_with_children_and_pet() {
        Human claraS = new Woman("Klara", "Stevenson", "08/05/1965");
        Human adamM = new Man("Adam", "Mendel", "12/07/1965", 75);
        testFamily.addChildren(new Human[]{claraS,adamM});
        testFamily.addPet(cat);
        assertEquals("==========================================================================================================\n" +
                "MENDEL FAMILY\n" +
                "    father: {name = Simon, surname = Mendel, birthDate = 21/08/1910, iq = 0}\n" +
                "    mother: {name = Zoya, surname = Mendel, birthDate = 05/03/1915, iq = 0}\n" +
                "    children:\n" +
                "        woman: {name = Klara, surname = Stevenson, birthDate = 08/05/1965, iq = 0}\n" +
                "        man: {name = Adam, surname = Mendel, birthDate = 12/07/1965, iq = 75}\n" +
                "    pets:\n" +
                "        CAT {nickname = Dora, age = 1, trickLevel = 75, habits='[Plays, Messes up with dogs, Sleeps]'}", testFamily.toString());
    }

    @Test
    void hashCode_test() {
        Family sameFamily = new Family(father, mother);
        assertEquals(testFamily.hashCode(), sameFamily.hashCode());
    }

    void childMustBelongToFamily(Family family, Human child){
        assertTrue(family.getChildren().contains(child));
        assertEquals(family, child.getFamily());
    }
}