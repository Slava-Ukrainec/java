package hw.hw13;

import hw.hw13.entities.Family;
import hw.hw13.helpers.DayOfWeek;
import hw.hw13.entities.Schedule;
import hw.hw13.entities.people.Human;
import hw.hw13.entities.people.Woman;
import hw.hw13.entities.pets.DomesticCat;
import hw.hw13.entities.pets.Pet;
import hw.hw13.entities.pets.RoboCat;
import hw.hw13.helpers.DateConverter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static hw.hw13.TestHelper.generateLongTimeInPast;
import static org.junit.jupiter.api.Assertions.*;

class WomanTest {

    private Human person;
    @BeforeEach
    void beforeEach(){
        person = new Woman("Simon", "Mendel", "21/08/1910");
    }

    @Test
    void empty_constructor(){
        Human person = new Woman();
        assertEquals("no name", person.getName());
        assertEquals("no surname", person.getSurname());
        assertEquals(0, person.getBirthDate());
        assertEquals(0, person.getIq());
        assertEquals(new Schedule(), person.getSchedule());
        assertNull(person.getFamily());
    }

    @Test
    void constructor_name_only(){
        Human person = new Woman("Alex");
        assertEquals("Alex", person.getName());
        assertEquals("no surname", person.getSurname());
        assertEquals(0, person.getBirthDate());
        assertEquals(0, person.getIq());
        assertEquals(new Schedule(), person.getSchedule());
        assertNull(person.getFamily());
    }

    @Test
    void constructor_name_surname_year(){
        Human person = new Woman("Alex", "Cotler", "14/10/1965");
        assertEquals("Alex", person.getName());
        assertEquals("Cotler", person.getSurname());
        assertEquals(new DateConverter("14/10/1965").getTime(), person.getBirthDate());
        assertEquals(0, person.getIq());
        assertEquals(new Schedule(), person.getSchedule());
        assertNull(person.getFamily());
    }

    @Test
    void constructor_full(){
        Human person = new Woman("Alex", "Cotler", "14/10/1965", 74);
        assertEquals("Alex", person.getName());
        assertEquals("Cotler", person.getSurname());
        assertEquals(new DateConverter("14/10/1965").getTime(), person.getBirthDate());
        assertEquals(74, person.getIq());
        assertEquals(new Schedule(), person.getSchedule());
        assertNull(person.getFamily());
    }

    @Test
    void setSurname() {
        person.setName("Nazar");
        assertEquals("Nazar", person.getName());
    }

    @Test
    void setBirthdate_long() {
        Calendar birth = Calendar.getInstance();
        birth.set(1989, Calendar.FEBRUARY, 01);
        person.setBirthDate(birth.getTimeInMillis());
        assertEquals(birth.getTimeInMillis(), person.getBirthDate());
        birth.set(2025, Calendar.JUNE, 12);
        assertThrows(IllegalArgumentException.class, ()-> person.setBirthDate(birth.getTimeInMillis()));
    }

    @Test
    void setBirthdate_string() {
        person.setBirthDate("12/04/2007");
        assertEquals(new DateConverter("12/04/2007").getTime(), person.getBirthDate());
        assertThrows(IllegalArgumentException.class, ()->person.setBirthDate("20/05/2022"));
        assertThrows(IllegalArgumentException.class, ()->person.setBirthDate("12.04.2007"));
    }

    @Test
    void describeAge(){
        person.setBirthDate(generateLongTimeInPast(25,10,27));
        assertEquals(String.format("Years: %d, months: %d, days: %d", 25,10,27), person.describeAge());
    }

    @Test
    void setIq_valid() {
        person.setIq(90);
        assertEquals(90, person.getIq());
    }

    @Test
    void setIq_invalid() {
        person.setIq(74);
        person.setIq(103);
        assertEquals(74, person.getIq());
    }

    @Test
    void scheduleTask() {
        person.scheduleTask(DayOfWeek.MONDAY, "Go to the Monday gym");
        person.scheduleTask(DayOfWeek.TUESDAY, "Play with Dora");
        person.scheduleTask(DayOfWeek.WEDNESDAY, "Go to the gym");
        person.scheduleTask(DayOfWeek.THURSDAY, "Clean the dishes");
        person.scheduleTask(DayOfWeek.FRIDAY, "Go to the gym once again");
        person.scheduleTask(DayOfWeek.SATURDAY, "Meet with friends");
        person.scheduleTask(DayOfWeek.SUNDAY, "Go to church");
        Map<String, String> sc = person.getSchedule().getSchedule();

        assertEquals(7, sc.size());
        Set<String> testKeys = new HashSet<>(Arrays.asList("MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY", "SUNDAY"));
        assertEquals(testKeys, sc.keySet());
        assertTrue(sc.values().contains("Go to the Monday gym"));
        assertTrue(sc.values().contains("Play with Dora"));
        assertTrue(sc.values().contains("Go to the gym"));
        assertTrue(sc.values().contains("Clean the dishes"));
        assertTrue(sc.values().contains("Go to the gym once again"));
        assertTrue(sc.values().contains("Meet with friends"));
        assertTrue(sc.values().contains("Go to church"));
    }

    @Test
    void setFamily() {
        Human mother = new Woman("Zoya", "Mendel", "05/03/1915");
        Family testFamily = new Family(person,mother);
        person.setFamily(testFamily);
        assertEquals(testFamily, person.getFamily());
    }

    @Test
    void resetFamily() {
        person.resetFamily();
        assertNull(person.getFamily());
    }

    @Test
    void feedPet_in_time() {
        Human mother = new Woman("Zoya", "Mendel", "05/03/1915");
        Family testFamily = new Family(person,mother);
        Pet cat = new DomesticCat("Dora", 1, 75, "Plays", "Sleeps", "Messes up with dogs");
        testFamily.addPet(cat);
        Pet robo = new RoboCat("Dora", 1, 75);
        assertFalse(person.feedPet(robo,true));
        assertTrue(person.feedPet(cat,true));
    }


    @Test
    void testToString() {
        Human person = new Woman("Simon", "Mendel", "21/08/1910", 85);
        assertEquals("Woman{name = Simon, surname = Mendel, birthDate = 21/08/1910, iq = 85}",
                person.toString());
    }

    @Test
    void equals_one_obj() {
        Human newPerson = person;
        assertEquals(person, newPerson);
    }

    @Test
    void equals_duplicate() {
        Human newPerson = new Woman("Simon", "Mendel", "21/08/1910");
        assertEquals(person, newPerson);
    }

    @Test
    void equals_null() {
        assertNotEquals(null, person);
    }

    @Test
    void equals_another_year() {
        Human newPerson = new Woman("Simon", "Mendel", "25/11/1911");
        assertNotEquals(person, newPerson);
    }

    @Test
    void equals_another_iq() {
        Human newPerson = new Woman("Simon", "Mendel", "21/08/1910", 50);
        assertNotEquals(person, newPerson);
    }

    @Test
    void equals_another_name() {
        Human newPerson = new Woman("Alex", "Mendel", "21/08/1910");
        assertNotEquals(person, newPerson);
    }

    @Test
    void equals_another_name_null() {
        Human newPerson = new Woman(null, "Mendel", "21/08/1910");
        assertNotEquals(person, newPerson);
    }

    @Test
    void equals_another_surname() {
        Human newPerson = new Woman("Simon", "Petliura", "21/08/1910");
        assertNotEquals(person, newPerson);
    }

    @Test
    void equals_another_surname_null() {
        Human newPerson = new Woman("Simon", null, "25/11/1911");
        assertNotEquals(person, newPerson);
    }

    @Test
    void equals_different_schedule() {
        Human newPerson = new Woman("Simon", "Mendel", "21/08/1910");
        person.scheduleTask(DayOfWeek.MONDAY, "Go to the Monday gym");
        person.scheduleTask(DayOfWeek.TUESDAY, "Play with Dora");
        assertEquals(person, newPerson);
    }

    @Test
    void equals_different_family() {
        Human mother = new Woman("Zoya", "Mendel", "05/03/1915");
        Family testFamily = new Family(person,mother);
        person.setFamily(testFamily);
        Human newPerson = new Woman("Simon", "Mendel", "21/08/1910");
        assertEquals(person, newPerson);
    }

    @Test
    void hashCode_test() {
        Woman sameWoman = new Woman("Simon", "Mendel", "21/08/1910");
        assertEquals(sameWoman.hashCode(), person.hashCode());
    }
}