package hw.hw13;

import hw.hw13.helpers.DateConverter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DateConverterTest {
    private DateConverter adaptedDate1;
    private DateConverter adaptedDate2;
    private DateConverter adaptedDate3;


    @BeforeEach
    void setUp(){
        adaptedDate1 = new DateConverter("01/08/2000");
        adaptedDate2 = new DateConverter(100000000000L);
        adaptedDate3 = new DateConverter(0);
    }

    @Test
    void getTime() {
        assertEquals(965077200000L, adaptedDate1.getTime());
        assertEquals(100000000000L, adaptedDate2.getTime());
        assertEquals(0, adaptedDate3.getTime());
    }

    @Test
    void toString_test() {
        assertEquals("01/08/2000", adaptedDate1.toString());
        assertEquals("03/03/1973", adaptedDate2.toString());
        assertEquals("not specified", adaptedDate3.toString());
    }
}