package hw.hw13.service;

import hw.hw13.entities.Family;
import hw.hw13.entities.people.Human;
import hw.hw13.entities.people.Man;
import hw.hw13.entities.people.Woman;
import hw.hw13.entities.pets.Dog;
import hw.hw13.entities.pets.DomesticCat;
import hw.hw13.entities.pets.Pet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FamilyServiceTest {
    private FamilyService service;
    private Family f1;
    private Family f2;
    private Family f3;
    private Family outsideFamily;
    private Human child1;
    private Human child2;
    private Human child3;

    @BeforeEach
    void setUp() {
        service = new FamilyService();
        Human antonS = new Man("Anton", "Syvokha", "20/02/1968");
        Human anastS = new Woman("Anastasiia", "Syvokha", "28/03/1972");
        Human serhiyP = new Man("Serhiy", "Prytula", "17/05/1988");
        Human mariiaP = new Woman("Mariia", "Prytula", "20/06/1991");
        Human alexK = new Man("Alexey", "Khvostenko", "15/04/1985");
        Human dariiaK = new Woman("Daria", "Khvostenko", "18/09/1990");
        Human zoyaM = new Woman("Zoya", "Mendel", "05/03/1915");
        Human simonM = new Man("Simon", "Mendel", "21/08/1910");
        child1 = new Man("Nazar", "Khvostenko", "05/01/2000");
        child2 = new Woman("Oksana", "Khvostenko", "12/11/2008");
        child3 = new Woman("Olena", "Syvokha", "14/12/1997");

        service.createNewFamily(antonS, anastS);
        service.createNewFamily(serhiyP, mariiaP);
        service.createNewFamily(alexK, dariiaK);
        f1 = service.getFamilyByIndex(0);
        service.adoptChild(f1, child3);
        f2 = service.getFamilyByIndex(1);
        f3 = service.getFamilyByIndex(2);
        service.adoptChild(f3, child1);
        service.adoptChild(f3, child2);

        outsideFamily = new Family(simonM, zoyaM);
    }

    @Test
    void getAllFamilies() {
        List<Family> families = service.getAllFamilies();
        assertEquals(3, families.size());
        listConsistsOf(families, f1, f2, f3);
    }

    @Test
    void getFamiliesBiggerThan() {
        listConsistsOf(service.getFamiliesBiggerThan(2), f1, f3);
        listConsistsOf(service.getFamiliesBiggerThan(-2), f1, f2, f3);
        listConsistsOf(service.getFamiliesBiggerThan(0), f1, f2, f3);
        assertTrue(service.getFamiliesBiggerThan(10).isEmpty());
    }

    @Test
    void getFamiliesLessThan() {
        listConsistsOf(service.getFamiliesLessThan(4), f2, f1);
        assertTrue(service.getFamiliesLessThan(-2).isEmpty());
        assertTrue(service.getFamiliesLessThan(0).isEmpty());
        listConsistsOf(service.getFamiliesLessThan(10), f1, f2, f3);
    }

    @Test
    void countFamiliesWithMemberNumber() {
        service.bornChild(f2, "Navka", "Orest");
        assertEquals(2, service.countFamiliesWithMemberNumber(3));
        assertEquals(0, service.countFamiliesWithMemberNumber(-5));
        assertEquals(0, service.countFamiliesWithMemberNumber(0));
        assertEquals(1, service.countFamiliesWithMemberNumber(4));

    }

    @Test
    void deleteFamilyByIndex() {
        assertTrue(service.deleteFamilyByIndex(0));
        listConsistsOf(service.getAllFamilies(), f2, f3);
        assertFalse(service.deleteFamilyByIndex(-1));
        listConsistsOf(service.getAllFamilies(), f2, f3);
        assertFalse(service.deleteFamilyByIndex(4));
        listConsistsOf(service.getAllFamilies(), f2, f3);
    }

    @Test
    void deleteFamily() {
        assertTrue(service.deleteFamily(f1));
        listConsistsOf(service.getAllFamilies(), f2, f3);
        assertFalse(service.deleteFamily(null));
        listConsistsOf(service.getAllFamilies(), f2, f3);
        assertFalse(service.deleteFamily(outsideFamily));
        listConsistsOf(service.getAllFamilies(), f2, f3);
    }

    @Test
    void bornChild() {
        assertEquals(f2, service.bornChild(f2, "Navka", "Orest"));
        familyHasChildren(f2, true);
        assertNull(service.bornChild(null, "Natalka", "Petro"));
        assertNull(service.bornChild(f2, null, "Petro"));
        assertNull(service.bornChild(f2, "Natalka", null));
        familyHasChildren(f2, true);
        assertEquals(f3, service.bornChild(f3, "Navka", "Orest"));
        familyHasChildren(f3, true, child1, child2);
    }

    @Test
    void adoptChild() {
        assertEquals(f2, service.adoptChild(f2, child3));
        familyHasChildren(f2, false, child3);
        assertNull(service.adoptChild(null, child1));
        assertNull(service.adoptChild(f2, null));
        familyHasChildren(f2, false, child3);
        assertEquals(f3, service.adoptChild(f3, child3));
        familyHasChildren(f3, false, child1, child2, child3);

    }

    @Test
    void deleteAllChildrenOlderThan() {
        assertThrows(IllegalArgumentException.class, ()->service.deleteAllChildrenOlderThan(-1));
        assertThrows(IllegalArgumentException.class, ()->service.deleteAllChildrenOlderThan(0));
        assertThrows(IllegalArgumentException.class, ()->service.deleteAllChildrenOlderThan(12));
        service.deleteAllChildrenOlderThan(18);
        familyHasChildren(f3, false, child2);
        assertTrue(f1.getChildren().isEmpty());
    }

    @Test
    void count() {
        assertEquals(3, service.count());
        service.deleteFamilyByIndex(1);
        assertEquals(2, service.count());
    }

    @Test
    void getFamilyByIndex() {
        assertNull(service.getFamilyByIndex(-1));
        assertEquals(f1, service.getFamilyByIndex(0));
        assertEquals(f2, service.getFamilyByIndex(1));
        assertEquals(f3, service.getFamilyByIndex(2));
        assertNull(service.getFamilyByIndex(3));
    }

    @Test
    void getPets() {
        assertNull(service.getPets(-1));
        assertNull(service.getPets(5));

        Pet dora = new DomesticCat("Dora", 1, 75, "Plays", "Sleeps", "Messes up with dogs");
        service.addPet(0, dora);

        assertTrue(service.getPets(0).contains(dora));
        assertTrue(service.getPets(1).isEmpty());
    }

    @Test
    void addPet() {
        Pet dora = new DomesticCat("Dora", 1, 75, "Plays", "Sleeps", "Messes up with dogs");
        Pet pluto = new Dog("Pluto", 2, 40, "Runs", "Jumps", "Plays with master");

        assertThrows(IllegalArgumentException.class, ()->service.addPet(-1, dora));
        assertThrows(IllegalArgumentException.class, ()->service.addPet(5, dora));
        assertThrows(IllegalArgumentException.class, ()->service.addPet(0, null));
        service.addPet(0, dora);
        assertTrue(service.getPets(0).contains(dora));
        service.addPet(0, pluto);
        assertTrue(service.getPets(0).contains(pluto));
        assertTrue(service.getPets(0).contains(dora));
        assertEquals(2, service.getPets(0).size());
    }

    void listConsistsOf(List<Family> list, Family... families) {
        assertEquals(list.size(), families.length);
        for (Family family: families) {
            System.out.println(family);
            assertTrue(list.contains(family));
        }
    }

    void familyHasChildren(Family family, Boolean plusOne, Human... children) {
        int additionalChild = plusOne ? 1 : 0;
        assertEquals(family.getChildren().size(), children.length + additionalChild);
        for (Human child: children) {
            System.out.println(child);
            assertTrue(family.getChildren().contains(child));
        }
    }

}