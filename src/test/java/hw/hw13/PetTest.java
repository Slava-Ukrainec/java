package hw.hw13;

import hw.hw13.entities.pets.DomesticCat;
import hw.hw13.entities.pets.Pet;
import hw.hw13.entities.pets.RoboCat;
import hw.hw13.entities.pets.Species;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PetTest {

    Pet cat;

    @BeforeEach
    void beforeEach() {
        cat = new DomesticCat("Dora", 1, 75, "Plays", "Sleeps", "Messes up with dogs");
    }

    @Test
    void constructor_species_name(){
        Pet cat = new DomesticCat ("Timka");
        assertEquals(Species.CAT, cat.getSpecies());
        assertEquals("Timka", cat.getNickname());
        assertEquals(0, cat.getAge());
        assertEquals(0, cat.getTrickLevel());
        assertEquals(0, cat.getHabits().size());
    }

    @Test
    void constructor_with_age_and_trickLevel(){
        Pet cat = new DomesticCat ("Timka",2, 75);
        assertEquals(Species.CAT, cat.getSpecies());
        assertEquals("Timka", cat.getNickname());
        assertEquals(2, cat.getAge());
        assertEquals(75, cat.getTrickLevel());
        assertEquals(0, cat.getHabits().size());
    }

    @Test
    void constructor_full(){
        assertEquals(Species.CAT, cat.getSpecies());
        assertEquals("Dora", cat.getNickname());
        assertEquals(1, cat.getAge());
        assertEquals(75, cat.getTrickLevel());
        assertEquals(3, cat.getHabits().size());
        assertTrue(cat.getHabits().contains("Plays"));
        assertTrue(cat.getHabits().contains("Sleeps"));
        assertTrue(cat.getHabits().contains("Messes up with dogs"));
    }

    @Test
    void setAge() {
        cat.setAge(2);
        assertEquals(2, cat.getAge());
    }

    @Test
    void setTrickLevel_valid() {
        cat.setTrickLevel(85);
        assertEquals(85, cat.getTrickLevel());
    }

    @Test
    void setTrickLevel_invalid() {
        cat.setTrickLevel(105);
        assertEquals(75, cat.getTrickLevel());
        cat.setTrickLevel(-2);
        assertEquals(75, cat.getTrickLevel());
    }

    @Test
    void addHabits() {
        cat.addHabits("Sleeps all day long");
        assertTrue(cat.getHabits().contains("Sleeps all day long"));
        assertEquals(4, cat.getHabits().size());
        cat.addHabits("Sleeps all day long", "Lies on the sun");
        assertEquals(5, cat.getHabits().size());
        assertTrue(cat.getHabits().contains("Sleeps all day long"));
        assertTrue(cat.getHabits().contains("Lies on the sun"));
    }

    @Test
    void testToString() {
        assertEquals("DomesticCat{species = CAT, nickname = Dora, age = 1, trickLevel = 75, habits='[Plays, Messes up with dogs, Sleeps]'}",cat.toString());
    }

    @Test
    void equals_one_obj() {
        Pet newCat = cat;
        assertEquals(cat, newCat);
    }

    @Test
    void equals_null() {
        assertNotEquals(cat, null);
    }

    @Test
    void equals_duplicate() {
        Pet newCat = new DomesticCat("Dora", 1, 75, "Plays", "Sleeps", "Messes up with dogs");
        assertEquals(cat, newCat);
    }

    @Test
    void equals_dif_tricklevel() {
        Pet newCat = new DomesticCat("Dora", 1, 50, "Plays", "Sleeps", "Messes up with dogs");
        assertNotEquals(cat, newCat);
    }

    @Test
    void equals_dif_species() {
        Pet newCat = new RoboCat("Dora", 1, 75, "Plays", "Sleeps", "Messes up with dogs");
        assertNotEquals(cat, newCat);
    }

    @Test
    void equals_dif_name() {
        Pet newCat = new DomesticCat("Felix", 1, 75, "Plays", "Sleeps", "Messes up with dogs");
        assertNotEquals(cat, newCat);
    }

    @Test
    void equals_dif_name_null() {
        Pet newCat = new DomesticCat(null,  1, 75, "Plays", "Sleeps", "Messes up with dogs");
        assertNotEquals(cat, newCat);
    }

    @Test
    void test_hashCode() {
        Pet newCat = new DomesticCat("Dora", 1, 75, "Plays", "Sleeps", "Messes up with dogs");
        assertEquals(cat.hashCode(), newCat.hashCode());
    }
}