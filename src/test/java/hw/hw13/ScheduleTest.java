package hw.hw13;

import hw.hw13.helpers.DayOfWeek;
import hw.hw13.entities.Schedule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class ScheduleTest {
    
    private Schedule schedule;

    
    @BeforeEach
    void beforeEach(){
        schedule = new Schedule();
    }

    @Test
    void defineTasks() {
        schedule.defineTasks(DayOfWeek.MONDAY, "Go to the Monday gym");
        schedule.defineTasks(DayOfWeek.TUESDAY, "Play with Dora");
        schedule.defineTasks(DayOfWeek.WEDNESDAY, "Go to the gym");
        schedule.defineTasks(DayOfWeek.THURSDAY, "Clean the dishes");
        schedule.defineTasks(DayOfWeek.FRIDAY, "Go to the gym once again");
        schedule.defineTasks(DayOfWeek.SATURDAY, "Meet with friends");
        schedule.defineTasks(DayOfWeek.SUNDAY, "Go to church");
        Map<String, String> sc = schedule.getSchedule();


        assertEquals(7, sc.size());
        Set<String> testKeys = new HashSet<>(Arrays.asList("MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY", "SUNDAY"));
        assertEquals(testKeys, sc.keySet());
        assertTrue(sc.values().contains("Go to the Monday gym"));
        assertTrue(sc.values().contains("Play with Dora"));
        assertTrue(sc.values().contains("Go to the gym"));
        assertTrue(sc.values().contains("Clean the dishes"));
        assertTrue(sc.values().contains("Go to the gym once again"));
        assertTrue(sc.values().contains("Meet with friends"));
        assertTrue(sc.values().contains("Go to church"));

    }

    @Test
    void test_toString() {
        assertEquals("[" +
                        "MONDAY: Task for MONDAY wasn't specified yet;\n" +
                        "TUESDAY: Task for TUESDAY wasn't specified yet;\n" +
                        "WEDNESDAY: Task for WEDNESDAY wasn't specified yet;\n" +
                        "THURSDAY: Task for THURSDAY wasn't specified yet;\n" +
                        "FRIDAY: Task for FRIDAY wasn't specified yet;\n" +
                        "SATURDAY: Task for SATURDAY wasn't specified yet;\n" +
                        "SUNDAY: Task for SUNDAY wasn't specified yet;" +
                        "]",
                schedule.toString());
    }

    @Test
    void equals_same_obj() {
        Schedule sc = schedule;
        assertEquals(schedule, sc);
    }

    @Test
    void equals_duplicate() {
        Schedule sc = new Schedule();
        assertEquals(schedule, sc);
    }

    @Test
    void equals_null() {
        assertNotEquals(schedule, null);
    }

    @Test
    void equals_properties() {
        Schedule aSc = new Schedule();

        schedule.defineTasks(DayOfWeek.MONDAY, "Go to the Monday gym");
        assertNotEquals(aSc, schedule);
        schedule = new Schedule();

        schedule.defineTasks(DayOfWeek.TUESDAY, "Play with Dora");
        assertNotEquals(aSc, schedule);
        schedule = new Schedule();

        schedule.defineTasks(DayOfWeek.WEDNESDAY, "Go to the gym");
        assertNotEquals(aSc, schedule);
        schedule = new Schedule();

        schedule.defineTasks(DayOfWeek.THURSDAY, "Clean the dishes");
        assertNotEquals(aSc, schedule);
        schedule = new Schedule();

        schedule.defineTasks(DayOfWeek.FRIDAY, "Go to the gym once again");
        assertNotEquals(aSc, schedule);
        schedule = new Schedule();

        schedule.defineTasks(DayOfWeek.SATURDAY, "Meet with friends");
        assertNotEquals(aSc, schedule);
        schedule = new Schedule();

        schedule.defineTasks(DayOfWeek.SUNDAY, "Go to church");
        assertNotEquals(aSc, schedule);
        schedule = new Schedule();
    }
}