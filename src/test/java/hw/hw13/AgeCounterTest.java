package hw.hw13;

import hw.hw13.helpers.AgeCounter;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static hw.hw13.TestHelper.generateLongTimeInPast;
import static org.junit.jupiter.api.Assertions.*;

class AgeCounterTest {
    static private AgeCounter counter1;
    static private AgeCounter counter2;
    static private AgeCounter counter3;
    static private AgeCounter counter4;
    static private AgeCounter counter5;
    static private AgeCounter counter6;

    @BeforeAll
    static void setUp() {
        counter1 = new AgeCounter(generateLongTimeInPast(25,10,27));
        counter2 = new AgeCounter(generateLongTimeInPast(8,0,0));
        counter3 = new AgeCounter(generateLongTimeInPast(9,1,0));
        counter4 = new AgeCounter(generateLongTimeInPast(10,11,5));
        counter5 = new AgeCounter(generateLongTimeInPast(11,0,27));
        counter6 = new AgeCounter(generateLongTimeInPast(12,2,11));
    }

    @Test
    void constructor(){
        assertThrows(IllegalArgumentException.class, ()->new AgeCounter(10000000000000000L));
        assertThrows(IllegalArgumentException.class, ()->new AgeCounter(0));
        assertThrows(IllegalArgumentException.class, ()->new AgeCounter(new Date().getTime()));
    }

    @Test
    void describeAge() {
        assertEquals(String.format("Years: %d, months: %d, days: %d", 25,10,27),counter1.describeAge());
        assertEquals(String.format("Years: %d, months: %d, days: %d", 8,0,0),counter2.describeAge());
        assertEquals(String.format("Years: %d, months: %d, days: %d", 9,1,0),counter3.describeAge());
        assertEquals(String.format("Years: %d, months: %d, days: %d", 10,11,5),counter4.describeAge());
        assertEquals(String.format("Years: %d, months: %d, days: %d", 11,0,27),counter5.describeAge());
        assertEquals(String.format("Years: %d, months: %d, days: %d", 12,2,11),counter6.describeAge());
    }

    @Test
    void getFullYears() {
        assertEquals(25, counter1.getFullYears());
        assertEquals(8, counter2.getFullYears());
        assertEquals(9, counter3.getFullYears());
        assertEquals(10, counter4.getFullYears());
        assertEquals(11, counter5.getFullYears());
        assertEquals(12, counter6.getFullYears());
    }





}