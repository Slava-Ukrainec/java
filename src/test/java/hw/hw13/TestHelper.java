package hw.hw13;

import java.util.Calendar;

import static java.util.Calendar.*;

public class TestHelper {

    // Method for testing purposes, generates long value for creating specific date in the past with
    // pre-defined year, months, days difference vs. current day
    public static long generateLongTimeInPast(int yearDif, int monthsDif, int daysDif) {
        int year;
        int month;
        int day;

        Calendar now = Calendar.getInstance();
        Calendar tempDate = Calendar.getInstance();

        int daysDelta = now.get(DAY_OF_MONTH) - daysDif;

        if (daysDelta < 0) {
            monthsDif += 1;
        }
        if (now.get(MONTH) - monthsDif < 0) {
            yearDif += 1;
            month = now.get(MONTH) - monthsDif + 12;
        } else {
            month = now.get(MONTH) - monthsDif;
        }
        year = now.get(YEAR) - yearDif;
        tempDate.set(year, month, 1);

        if (daysDelta < 0) {
            day = tempDate.getActualMaximum(DAY_OF_MONTH) + daysDelta;
        } else {
            day = now.get(DAY_OF_MONTH) - daysDif;
        }
        tempDate.set(year,month,day);
        return tempDate.getTimeInMillis();
    }
}
