package cw.javaBasic.Lesson10.Printer;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PrintFormat2Test {

    @Test
    void format() {
        PrintFormat1 format = new PrintFormat1();
//        assertEquals("=================\nHello\n=================\n", format.format("Hello"));
        assertEquals("Hello", format.format("Hello"));
    }
}