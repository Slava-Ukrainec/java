package cw.javaBasic.Lesson8.InterfacePractice;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class AddTest {

    static Add checkVariable;
    Add add;

    @BeforeAll
    static void before() {
       checkVariable = new Add(1,2);
    }

    @BeforeEach
    void beforeEach() {
       add = new Add(1,2);
    }

    @Test
    void operation() {
        assertEquals(3, add.operation());
    }

    @AfterEach
    void afterEach(){
        add = null;
    }

    @AfterAll
    static void afterAll(){
        checkVariable = null;
    }

}