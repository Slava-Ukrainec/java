package hw.hw13.ui;

import java.util.ArrayList;
import java.util.Arrays;

public class StrValidator {
    private String value;
    private String errMsg;
    private ArrayList<Integer> validations = new ArrayList<>();
    private ArrayList<String> options;

    private static final int NOT_EMPTY = 0;
    private static final int FROM_OPTIONS = 1;

    private boolean inputIsRelevant(){
        return validations.stream().noneMatch(this::doValidation);
    }

    private boolean doValidation(int validation){
        switch (validation) {
            case NOT_EMPTY:
                return invalidEmpty();
            case FROM_OPTIONS:
                return invalidOption();
                default:
                    throw new IllegalArgumentException("Unknown validation rule");
        }
    }

    private boolean invalidEmpty(){
        if (value.equals("")){
            errMsg = "Please, provide input";
            return true;
        } else {
            return false;
        }
    }

    private boolean invalidOption() {
        if (!options.contains(value.toLowerCase())){
            StringBuilder sb = new StringBuilder();
            sb.append("Invalid input, please put one of values:\n");
            options.forEach(option->sb.append(option + "\n"));
            errMsg = sb.toString();
            return true;
        } else
            return false;
    }

    public StrValidator notEmpty() {
        validations.add(NOT_EMPTY);
        return this;
    }

    public StrValidator fromOptions (String...options){
        validations.add(FROM_OPTIONS);
        this.options = new ArrayList<>(Arrays.asList(options));
        return this;
    }

    public String getErrorMessage() {
        return errMsg;
    }

    public boolean accepts(String input){
        this.value = input;
        return inputIsRelevant();
    }
}
