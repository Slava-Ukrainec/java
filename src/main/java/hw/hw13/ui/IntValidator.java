package hw.hw13.ui;

import java.util.ArrayList;

public class IntValidator {
    private int value;
    private String errMsg;
    private int minValue;
    private int maxValue;
    private ArrayList<Integer> validations = new ArrayList<>();

    private static final int NOT_ZERO = 0;
    private static final int WITHIN_RANGE = 1;

    private boolean inputIsRelevant(){
        return validations.stream().noneMatch(this::checkViolations);
    }

    private boolean checkViolations(int validation){
        switch (validation) {
            case NOT_ZERO:
                return invalidNotZero();
            case WITHIN_RANGE:
                return invalidRange();
                default:
                    throw new IllegalArgumentException("Unknown validation rule");
        }
    }

    private boolean invalidNotZero(){
        if (value == 0){
            errMsg = "Please, provide not zero number";
            return true;
        } else {
            return false;
        }
    }

    private boolean invalidRange(){
        if (value < minValue | value > maxValue) {
            errMsg = String.format("Please, provide number from %d to %d", minValue, maxValue);
            return true;
        } else {
            return false;
        }
    }

    public IntValidator notZero() {
        validations.add(NOT_ZERO);
        return this;
    }

    public IntValidator withinRange(int min, int max){
        this.minValue = min;
        this.maxValue = max;
        validations.add(WITHIN_RANGE);
        return this;
    }

    public boolean accepts(int input){
        this.value = input;
        return inputIsRelevant();
    }

    public String getErrorMessage(){
        return errMsg;
    }

}
