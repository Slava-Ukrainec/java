package hw.hw13.ui;

import hw.hw13.exceptions.ExitException;
import hw.hw13.logger.Logger;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class ConsoleManager {
    private static Scanner sc = new Scanner(System.in);
    private static final String EXIT = "exit";
    private Logger logger = Logger.getInstance();

    private void checkForExit(String input){
        if (input.toLowerCase().equals(EXIT)) {
            throw new ExitException();
        }
    }

    public void showMessage(String message){
        System.out.println(message);
    }

    public String pause(){
        return getStringValue("\nPress enter to continue...");
    }

    public String getStringValue(String... requestMessages){
        for (String msg: requestMessages) {
            showMessage(msg);
        }
            String input = sc.nextLine();
            if (!input.equals("")){
                logger.info("*** USER INPUT: " + input);
            }
            checkForExit(input);
            return input;
    }

    public String getStringValue(StrValidator validator, String... requestMessages){
        for (String msg: requestMessages) {
            showMessage(msg);
        }
        for (;;){
            String input = getStringValue();
            if (validator.accepts(input)){
                return input;
            } else {
                showMessage(validator.getErrorMessage());
            }
        }
    }

    public int getIntValue(String... requestMessages){
        for (String msg: requestMessages) {
            showMessage(msg);
        }
        for (;;){
            try {
                return Integer.parseInt(getStringValue());
            } catch (NumberFormatException e) {
                showMessage("Please, enter number");
            }
        }
    }

    public int getIntValue(IntValidator validator, String... requestMessages){
        for (String msg: requestMessages) {
            showMessage(msg);
        }
        for (;;){
            int input = getIntValue();
            if (validator.accepts(input)){
                return input;
            } else {
                System.out.println(validator.getErrorMessage());
            }
        }
    }

    // adapts user index that starts from 1 to collection index that starts from 0
    public int requestCollectionIndex(int colSize, String... requestMsgs) {
        return getIntValue(new IntValidator().withinRange(1, colSize), requestMsgs) - 1;
    }


    public int getMenuChoice(ArrayList<String> menuItems){
        menuItems.forEach(this::showMessage);
        showMessage("\n");
        showMessage("Please, select number from 1 to " + menuItems.size());
        return getIntValue(new IntValidator().withinRange(1,menuItems.size()));
    }
    
    public int getMainMenuChoice(){
        ArrayList<String> menuItems = new ArrayList<>();
        menuItems.add("1. Fill with test data");
        menuItems.add("2. Show all families");
        menuItems.add("3. Get all families larger than specified quantity");
        menuItems.add("4. Get all families smaller than specified quantity");
        menuItems.add("5. Get all families of specific size");
        menuItems.add("6. Create new family");
        menuItems.add("7. Delete family by index");
        menuItems.add("8. Edit family");
        menuItems.add("9. Delete all children older than specified age");
        menuItems.add("10. Load data from storage");
        menuItems.add("11. Save changes to storage");
        return getMenuChoice(menuItems);
    }

    public String getDateString (String person) {
        showMessage("What year " + person + " was born at");
        int birthYear = getIntValue(new IntValidator().notZero());
        showMessage("What month " + person + " was born at (put number from 1 to 12)");
        int birthMonth = getIntValue(new IntValidator().withinRange(1,12));
        showMessage("What day of month " + person + " was born at (also put as number)");
        int birthDay = getIntValue(new IntValidator().withinRange(1,31));
        return String.format("%d/%d/%d", birthDay, birthMonth, birthYear);
    }

    public LinkedList<String> getPersonalData (String person) {
        return new LinkedList<String>(){{
            add(getStringValue(new StrValidator().notEmpty(), "Put " + person + " name"));
            add(getStringValue(new StrValidator().notEmpty(), "Put " + person + " last name"));
            add(getStringValue(getDateString(person)));
        }};
    }

    public void showErrMsg(Exception e){
        showMessage("\n" + e.getMessage() + "\n");
    }

}
