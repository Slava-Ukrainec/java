package hw.hw13.entities;

import hw.hw13.helpers.DayOfWeek;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;


public class Schedule implements Serializable {

    private Map<String, String> schedule = new HashMap<>(7);

    public Schedule(){
        this.genSchedule();
    }

    private void genSchedule() {
        for (DayOfWeek day: DayOfWeek.values()) {
            schedule.put(day.name(), "Task for " + day.name() + " wasn't specified yet");
        }
    }

    public void defineTasks(DayOfWeek day, String tasks) {
        schedule.put(day.name(), tasks);
    }

    public String getTasks(DayOfWeek day) {
        return schedule.get(day.name());
    }

    public Map<String, String> getSchedule(){
        return this.schedule;
    }

    @Override
    public String toString() {
        StringJoiner sj = new StringJoiner("\n","[","]");

        for (DayOfWeek day: DayOfWeek.values()) {
            sj.add(String.format("%s: %s;", day, schedule.get(day.name())));
        }
        return sj.toString();
    }

    public static void main(String[] args) {
        Schedule schedule = new Schedule();
        System.out.println(schedule);
    }

    @Override
    public boolean equals(Object another) {
        if (this == another) return true;
        if (another == null) return false;
        if (this.getClass() != another.getClass()) return false;
        Schedule schedule = (Schedule) another;
        return this.getSchedule().equals(schedule.getSchedule());
    }

}
