package hw.hw13.entities;

import hw.hw13.entities.people.Human;
import hw.hw13.entities.people.Man;
import hw.hw13.entities.people.Woman;
import hw.hw13.entities.people.HumanCreator;
import hw.hw13.entities.pets.Pet;
import hw.hw13.exceptions.FamilyOverflowException;

import java.io.Serializable;
import java.util.*;

public class Family implements HumanCreator, Serializable {
    private final Human father;
    private final Human mother;
    private List<Human> children;
    private Set<Pet> pets = new HashSet<>();

    public static final int CHILDREN_LIMIT = 5;

    static {
        System.out.println("Class Family has been instantiated for the first time");
    }

    {
        System.out.println(String.format("New %s instance has been created", this.getClass().getSimpleName()));

    }

    public Family(Human father, Human mother) {
        this(father, mother, new Human[]{});
    }

    public Family(Human father, Human mother, Human[] children) {
        this(father, mother, children, null);
    }

    public Family(Human father, Human mother, Pet pet) {
        this(father, mother, new Human[]{}, pet);
    }

    public Family(Human father, Human mother, Human[] children, Pet pet){
        this(father, mother, Arrays.asList(children), pet);
    }

    public Family(Human father, Human mother, List<Human> children, Pet pet){
        father.setFamily(this);
        mother.setFamily(this);
        this.father = father;
        this.mother = mother;
        for (Human child : children) {
            child.setFamily(this);
        }
        this.children = new ArrayList<>(children);
        addPet(pet);
    }

    public Human getFather() {
        return father;
    }

    public Human getMother() {
        return mother;
    }

    public List<Human> getChildren() {
        return children;
    }

    public List<Human> bornChild(String girlName, String boyName){
        Random r = new Random();
        Human child;
        if (r.nextInt(11) < 5) {
            child = new Woman();
            child.setName(girlName);
        } else {
            child = new Man();
            child.setName(boyName);
        }

        child.setSurname(this.getFather().getSurname());
        child.setBirthDate(new Date().getTime());
        child.setIq(this.getMother().getIq()/2 + this.getFather().getIq()/2);
        addChild(child);
        return children;
    }

    public List<Human> addChild(Human child) {
        if (!children.contains(child) &
                getFather() != child &
                getMother()!= child) {
            children.add(child);
            child.setFamily(this);
        }
        return children;
    }

    public List<Human> addChildren(Human[] childrenArr) {
        return addChildren(Arrays.asList(childrenArr));
    }

    public List<Human> addChildren(List<Human> childrenArr) {
        childrenArr.forEach(this::addChild);
        return children;
    }

    public Set<Pet> getAllPets() {
        return pets;
    }

    public Pet getPet(String nickname){
        return pets
                .stream()
                .filter(pet -> pet.getNickname().equals(nickname))
                .findFirst()
                .get();
    }

    public Set<Pet> addPet(Pet pet) {
        if (pet != null){
            this.pets.add(pet);
        }
        return Collections.unmodifiableSet(this.pets);
    }

    public boolean deleteChild(Human child) {
        if (children.contains(child)){
            child.resetFamily();
            return children.remove(child);
        } else return false;
    }

    public boolean deleteChild(int pos) {
        if (pos >= 0 & pos < children.size()) {
            return deleteChild(children.get(pos));
        } else return false;
    }

    public void removePet(Pet pet) {
        this.pets.remove(pet);
    }

    public int countFamily() {
        return children.size() + 2;
    }

    @Override
    public boolean equals(Object another) {
        if (this == another) return true;
        if (another == null) return false;
        if (this.getClass() != another.getClass()) return false;
        Family family = (Family) another;
        if (!this.father.equals(family.father)) return false;
        if (this.mother.equals(family.mother)) return true;
        else return false;
    }

    @Override
    public int hashCode() {
        return father.hashCode() * 17 + mother.hashCode();
    }

    @Override
    public String toString() {

        return this.prettyFormat();
//        StringJoiner sj = new StringJoiner("\n", String.format("\n==========\n%s family\n",father.getSurname()), "\n==========");
//        sj.add(father.toString());
//        sj.add(mother.toString());
//
//        if(!children.isEmpty()){
//            sj.add("Children:");
//            children.forEach(child -> sj.add(child.toString()));
//        }
//        if (pets.size() > 0) {
//            sj.add("Pets:");
//            pets.forEach(pet->sj.add(pet.toString()));
//        }
//        return sj.toString();
    }

    public String prettyFormat() {
        StringJoiner sj = new StringJoiner("\n", String.format("==========================================================================================================\n%s FAMILY\n",father.getSurname().toUpperCase()),"");
        sj.add("    father: " + father.prettyFormat());
        sj.add("    mother: " + mother.prettyFormat());

        if(!children.isEmpty()){
            Collections.sort(children, (o1, o2) -> o2.getFullYears() - o1.getFullYears());
            sj.add("    children:");
            children.forEach(child -> {
                    String descr;
                    if (child.getClass() == Man.class) {
                        descr = child.getFullYears() < 18 ? "boy" : "man";
                    } else {
                        descr = child.getFullYears() < 18 ? "girl" : "woman";
                    }
                    sj.add("        " + descr + ": " + child.prettyFormat());
            });
        }
        if (pets.size() > 0) {
            sj.add("    pets:");
            pets.forEach(pet->sj.add("        " + pet.prettyFormat()));
        }
        return sj.toString();
    }

    @Override
    protected void finalize() {
        System.out.println("Launching deprecated finalize method for Family class instance");
    }

}

