package hw.hw13.entities.pets;

public final class RoboCat extends Pet implements doesHarm {
    public RoboCat(String nickname) {
        super(nickname);
    }

    public RoboCat(String nickname, int age, int trickLevel, String... habits) {
        super(nickname, age, trickLevel, habits);
    }

    public void respond() {
        System.out.println(String.format("Hi, master. I am %s. I was missing you!", nickname));
    }

    @Override
    public void foul(){
        System.out.println("Even though my mind is artificial, I still enjoy this");
    }
}
