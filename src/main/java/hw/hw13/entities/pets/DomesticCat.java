package hw.hw13.entities.pets;

public final class DomesticCat extends Pet implements doesHarm {
    public DomesticCat(String nickname) {
        super(nickname);
    }

    public DomesticCat(String nickname, int age, int trickLevel, String... habits) {
        super(nickname, age, trickLevel, habits);
    }

    public void respond() {
        System.out.println(String.format("Meow, master. I am %s. I was almost missing you!", nickname));
    }

    public static void main(String[] args) {
        Pet cat = new DomesticCat("Dora", 1, 75, "Plays", "Sleeps", "Messes up with dogs");
        System.out.println(cat);

    }
}
