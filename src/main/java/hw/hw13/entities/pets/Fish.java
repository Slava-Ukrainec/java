package hw.hw13.entities.pets;

public final class Fish extends Pet {
    public Fish(String nickname) {
        super(nickname);
    }

    public Fish(String nickname, int age, String... habits) {
        super(nickname, age, 0, habits);
    }

    public void respond() {
        System.out.println("...");
    }
}
