package hw.hw13.entities.pets;

import java.io.Serializable;
import java.util.*;

public abstract class Pet implements Serializable {
    protected final Species species;
    protected String nickname;
    protected int age;
    protected int trickLevel;
    protected Set<String> habits = new HashSet<>();

    static {
        System.out.println("Class Pet has been instantiated for the first time");
    }

    {
        if (this.getClass() == DomesticCat.class) this.species = Species.CAT;
        else if (this.getClass() == Dog.class) this.species = Species.DOG;
        else if (this.getClass() == Fish.class) this.species = Species.FISH;
        else if (this.getClass() == RoboCat.class) this.species = Species.ROBOCAT;
        else this.species = Species.UNKNOWN;
        System.out.println(String.format("New %s instance has been created", this.getClass().getSimpleName()));
    }

    Pet (String nickname) {
        this(nickname, 0, 0);
    }

    Pet(String nickname, int age, int trickLevel, String... habits) {
        this(nickname,  age, trickLevel, Arrays.asList(habits));
    }

    Pet(String nickname, int age, int trickLevel, List<String> habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits.addAll(habits);
    }


    public Species getSpecies() {
        return species;
    }

    public String getNickname() {
        return nickname;
    }

    public int getAge() {
        return age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public String getVerbalTrickLevel() {
        return getTrickLevel() > 50 ? "very tricky" : "almost not tricky";
    }

    public Set<String> getHabits() {
        return habits;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setTrickLevel(int trickLevel) {
        if (trickLevel <= 100 && trickLevel >= 0) {
            this.trickLevel = trickLevel;
        }
    }

    public void addHabits(String... habits) {
        for (String habit: habits) {
            this.habits.add(habit);
        }
    }

    public void removeHabits(String... habits) {
        for (String habit: habits) {
            this.habits.remove(habit);
        }
    }

    public void eat() {
        System.out.println("I am eating");
    }

    public abstract void respond();

    public boolean cheatHuman() {
        Random rand = new Random();
        int gamble = rand.nextInt(101);
        return trickLevel > gamble;
    }

    @Override
    public String toString(){
        StringJoiner petDescription = new StringJoiner(", ", String.format("%s{", this.getClass().getSimpleName()), "}");

        petDescription.add("species = " + getSpecies().name());
        petDescription.add("nickname = " + getNickname());
        petDescription.add("age = " + getAge());
        petDescription.add("trickLevel = " + getTrickLevel());

        StringJoiner habits = new StringJoiner(", ", "[", "]");
        this.habits.forEach(habits::add);
        petDescription.add("habits='" + habits.toString() + "'");

        return petDescription.toString();
    }

    public String prettyFormat(){
        StringJoiner petDescription = new StringJoiner(", ", String.format("%s {", getSpecies().name()), "}");
        petDescription.add("nickname = " + getNickname());
        petDescription.add("age = " + getAge());
        petDescription.add("trickLevel = " + getTrickLevel());

        StringJoiner habits = new StringJoiner(", ", "[", "]");
        this.habits.forEach(habits::add);
        petDescription.add("habits='" + habits.toString() + "'");

        return petDescription.toString();
    }

    @Override
    public boolean equals(Object another){
        if (this == another) return true;
        if (another == null) return false;
        if (this.getClass() != another.getClass()) return false;
        Pet pet = (Pet) another;
        if ( !(this.trickLevel == pet.trickLevel) ) return false;
        if (species != null) {
            if ( !this.species.equals(pet.species) ) return false;
        } else {
            if (pet.species != null) return false;
        }
        if (nickname != null) {
            if ( !this.nickname.equals(pet.nickname) ) return false;
        } else {
            if (pet.nickname != null) return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = species != null ? species.hashCode() : 0;
        result = 31 * result + (nickname != null ? nickname.hashCode() : 0);
        result = 31 * result + trickLevel;
        return result;
    }

    @Override
    protected void finalize() {
        System.out.println("Launching deprecated finalize method for Pet class instance");
    }


}
