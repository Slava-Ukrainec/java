package hw.hw13.entities.pets;

public enum Species {
    CAT("DomesticCat", false, 4, true),
    DOG("Dog", false, 4, true),
    TURTLE("Turtle", false, 4, false),
    SNAKE("Snake", false, 0, false),
    PARROT("Parrot", true, 2, false),
    HAMSTER("Hamster", false, 4, true),
    FISH("Fish", false, 0, false),
    ROBOCAT("RoboCat", false, 4, false),
    UNKNOWN("Unknown", false, 0, false);

    private final String name;
    private final boolean canFly;
    private final int legs;
    private final boolean hasFur;

    Species(String name, boolean canFly, int legs, boolean hasFur) {
        this.name = name;
        this.canFly = canFly;
        this.legs = legs;
        this.hasFur = hasFur;
    }

    public String getName() {
        return name;
    }

    public boolean isCanFly() {
        return canFly;
    }

    public int getLegs() {
        return legs;
    }

    public boolean isHasFur() {
        return hasFur;
    }
}
