package hw.hw13.entities.pets;

public final class Dog extends Pet implements doesHarm{
    public Dog(String nickname) {
        super(nickname);
    }

    public Dog(String nickname, int age, int trickLevel, String... habits) {
        super(nickname, age, trickLevel, habits);
    }

    public void respond() {
        System.out.println(String.format("Wooof, master. I am %s. I was missing you!", nickname));
    }
}
