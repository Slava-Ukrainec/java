package hw.hw13.entities.people;

import hw.hw13.entities.Family;
import hw.hw13.helpers.DayOfWeek;
import hw.hw13.entities.Schedule;
import hw.hw13.entities.pets.Pet;
import hw.hw13.helpers.AgeCounter;
import hw.hw13.helpers.DateConverter;

import java.io.Serializable;
import java.util.Date;
import java.util.StringJoiner;

public abstract class Human implements Serializable {
    protected String name;
    protected String surname;
    protected long birthDate;
    protected int iq;
    protected final Schedule schedule = new Schedule();
    protected Family family;

    static {
        System.out.println("Class Human has been instantiated for the first time");
    }

    {
        System.out.println(String.format("New %s instance has been created", this.getClass().getSimpleName()));

    }

    Human() {
        this("no name");
    }

    Human(String name) {
        this(name, "no surname", null);
    }

    Human(String name, String surname, String birthDate) {
        this(name, surname, birthDate, 0);
    }

    public Human(String name, String surname, String birthDate, int iq) {
        this.name = name;
        this.surname = surname;
        if (birthDate != null) {
            this.birthDate = new DateConverter(birthDate).getTime();
        } else {
            this.birthDate = 0;
        }
        this.iq = iq;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(long birthDate) {
        if (new Date(birthDate).after(new Date())) throw new IllegalArgumentException("Invalid birth date: future values not allowed");
        this.birthDate = birthDate;
    }

    public void setBirthDate(String formattedDate) {
        setBirthDate(new DateConverter(formattedDate).getTime());
    }

    public String describeAge(){
        if (birthDate != 0) {
            return new AgeCounter(birthDate).describeAge();
        } else return "Age is not specified";
    }

    public int getFullYears(){
        if (birthDate != 0) {
            return new AgeCounter(birthDate).getFullYears();
        }
        else return 0;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        if (iq >=0 && iq <=100) {
            this.iq = iq;
        }
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void scheduleTask(DayOfWeek day, String tasks){
        schedule.defineTasks(day, tasks);
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public void resetFamily() {
        this.family = null;
    }

    public Family getFamily() {
        return family;
    }

    public boolean feedPet(Pet pet, boolean isTimeToFeed) {
        if (family != null) {
            if (family.getAllPets().contains(pet)) {
                String nickname = pet.getNickname();
                if (isTimeToFeed) {
                    System.out.println(String.format("Oh, I need to feed %s", nickname));
                    return true;
                } else {
                    boolean fed = pet.cheatHuman();
                    String message = fed ?
                            String.format("Hmm, I would rather feed %s", nickname)
                            : String.format("%s doesn't seem hungry", nickname);
                    System.out.println(message);
                    return fed;
                }
            } else {
                System.out.println("There are no pets in family");
                return false;
            }
        } else {
            System.out.println(String.format("%s doesn't have family and pets and will die alone", this.getName()));
            return false;
        }
    }

    public void describePet(Pet pet) {
        if (family != null) {
            if (family.getAllPets().contains(pet)) {
                String species = pet.getSpecies().getName();
                int age = pet.getAge();
                String tricknessLevel = pet.getVerbalTrickLevel();
                System.out.println(String.format("I have a %s, it is %d birthDates old, it's %s", species, age, tricknessLevel));
            } else {
                System.out.println("There are no pets in family");
            }
        } else {
            System.out.println(String.format("%s has now family and pet and will die alone", this.getName()));
        }
    }

    public void greetPet(Pet pet) {
        if (family != null) {
            if (family.getAllPets().contains(pet)) {
                System.out.println(String.format("Hi, %s!", pet.getNickname()));
            } else {
                System.out.println("There are no pets in family");
            }
        } else {
            System.out.println(String.format("%s has no family and pets and will die alone", this.getName()));
        }
    }

    @Override
    public String toString() {
        StringJoiner sj = new StringJoiner(", ", String.format("%s{", this.getClass().getSimpleName()), "}");
        sj.add("name = " + name);
        sj.add("surname = " + surname);
        sj.add("birthDate = " + new DateConverter(birthDate));
        sj.add("iq = " + iq);
        return sj.toString();
    }

    public String prettyFormat() {
        StringJoiner sj = new StringJoiner(", ", "{", "}");
        sj.add("name = " + name);
        sj.add("surname = " + surname);
        sj.add("birthDate = " + new DateConverter(birthDate));
        sj.add("iq = " + iq);
        return sj.toString();
    }

    @Override
    public boolean equals(Object another) {
        if (this == another) return true;
        if (another == null) return false;
        if (this.getClass() != another.getClass()) return false;
        Human human = (Human) another;
        if (!(this.birthDate == human.birthDate) || !(this.iq == human.iq)) return false;
        if (name != null) {
            if (!this.name.equals(human.name)) return false;
        } else {
            if (human.name != null) return false;
        }
        if (surname != null) {
            if (!this.surname.equals(human.surname)) return false;
        } else {
            if (human.surname != null) return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (int)birthDate;
        result = 31 * result + iq;
        return result;
    }


    @Override
    protected void finalize() {
        System.out.println("Launching deprecated finalize method for Human class instance");
    }

    public int getYear(){
        throw  new IllegalArgumentException("Method getYear must be deleted");
    }

    public void setYear (int year) {
        throw new IllegalArgumentException("Method setYear must be deleted");
    }


}
