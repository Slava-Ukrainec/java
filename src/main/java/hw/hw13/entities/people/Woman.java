package hw.hw13.entities.people;

import hw.hw13.entities.pets.Pet;

public final class Woman extends Human {
    public Woman() {
    }

    public Woman(String name) {
        super(name);
    }

    public Woman(String name, String surname, String birthDate) {
        super(name, surname, birthDate);
    }

    public Woman(String name, String surname, String birthDate, int iq) {
        super(name, surname, birthDate, iq);
    }

    @Override
    public void greetPet(Pet pet) {
        if (family != null) {
            if (family.getAllPets().contains(pet)) {
                System.out.println(String.format("Hello, my dear lovely %s", pet.getNickname()));
            } else {
                System.out.println("There are no pets in family");
            }
        } else {
            System.out.println(String.format("%s has no family and pets and will die alone", this.getName()));
        }
    }

    public void makeUp(){
        System.out.println("I'm going to put makeup, cause I'm a woman, you know");
    }

}
