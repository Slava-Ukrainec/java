package hw.hw13.entities.people;


import java.util.List;

public interface HumanCreator {
    List<Human> bornChild(String girlName, String boyName);
}
