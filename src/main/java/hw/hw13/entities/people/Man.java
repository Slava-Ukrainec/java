package hw.hw13.entities.people;

import hw.hw13.entities.pets.Pet;

public final class Man extends Human {

    public Man() {
    }

    public Man(String name) {
        super(name);
    }

    public Man(String name, String surname, String birthDate) {
        super(name, surname, birthDate);
    }

    public Man(String name, String surname, String birthDate, int iq) {
        super(name, surname, birthDate, iq);
    }

    @Override
    public void greetPet(Pet pet) {
        if (family != null) {
            if (family.getAllPets().contains(pet)) {
                System.out.println(String.format("It's you, %s, again it's you!", pet.getNickname()));
            } else {
                System.out.println("There are no pets in family");
            }
        } else {
            System.out.println(String.format("%s has no family and pets and will die alone", this.getName()));
        }
    }

    public void repairCar(){
        System.out.println("I'm going to repair car, cause I'm a man, you know");
    }

    public static void main(String[] args) {
        Human person = new Man("Simon", "Mendel", "15/02/1960");
        System.out.println(person.hashCode());
    }
}
