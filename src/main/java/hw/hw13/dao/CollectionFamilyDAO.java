package hw.hw13.dao;

import hw.hw13.entities.Family;
import hw.hw13.helpers.DBRandomizer;
import hw.hw13.logger.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CollectionFamilyDAO implements FamilyDAO {
    private List<Family> storage = new ArrayList<>();
    private File file = null;
    private String path;

    private static String DEFAULT_PATH = "./storage/family_data.bin";
    private Logger logger = Logger.getInstance();


    @Override
    public List<Family> getAllFamilies() {
        logger.info("running getAllFamilies method");
        return Collections.unmodifiableList(storage);

    }

    @Override
    public Family getFamilyByIndex(int index) {
        logger.info("running getFamilyByIndex with index " + index);
        if (index >= 0 & index < storage.size()) {
            return storage.get(index);
        } else throw new IllegalArgumentException("Invalid index");
    }

    @Override
    public boolean deleteFamily(int index) {
        logger.info("running deleteFamily with index " + index);
        if (index >= 0 & index < storage.size()) {
            storage.remove(index);
            return true;
        } else return false;

    }

    @Override
    public boolean deleteFamily(Family family) {
        logger.info("running deleteFamily for " + family.getFather().getSurname().toUpperCase() + " family");
        if (family != null) {
            return storage.remove(family);
        } else return false;
    }

    @Override
    public void saveFamily(Family family) {
        logger.info("running saveFamily for " + family.getFather().getSurname().toUpperCase() + " family");
        int index = storage.indexOf(family);
        if (index >= 0 & index < storage.size()){
            storage.set(index, family);
        } else {
            storage.add(family);
        }
    }

    private void connectWithFile() {
        if (file == null) {
            file = new File(path);
        }
    }

    private void createFile(String path) throws IOException{
        file = new File(path);
        if (!file.exists()) {
            file.createNewFile();
        }
        this.path = path;
    }

    @Override
    public String save(){
        return save(DEFAULT_PATH);
    }

    public String save(String path){
        try {
            createFile(path);
            logger.info("saving family data to file " + file.getPath());
            ObjectOutputStream out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
            out.writeObject(storage);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to save families data to file" + file.getPath());
        }
        return path;
    }

    @Override
    public void loadData(){
        try {
            connectWithFile();
            if (file.exists()) {
                logger.info("loading family data from file");
                ObjectInputStream in = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file)));
                storage = (ArrayList)in.readObject();
            } else throw new FileNotFoundException("Families storage file wasn't found");

        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to read families data from file");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void generateRandomData(){
        logger.info("generating random test data");
        this.storage = new DBRandomizer().getDB();
    }
}
