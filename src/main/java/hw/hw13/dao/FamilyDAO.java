package hw.hw13.dao;

import hw.hw13.entities.Family;

import java.util.List;

public interface FamilyDAO {
    List<Family> getAllFamilies();
    Family getFamilyByIndex(int index);
    boolean deleteFamily (int index);
    boolean deleteFamily (Family family);
    void saveFamily(Family family);
    void generateRandomData();
    String save();
    void loadData();
}
