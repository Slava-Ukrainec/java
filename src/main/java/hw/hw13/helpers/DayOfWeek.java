package hw.hw13.helpers;

public enum DayOfWeek {
    MONDAY(0),
    TUESDAY(1),
    WEDNESDAY(2),
    THURSDAY(3),
    FRIDAY(4),
    SATURDAY(5),
    SUNDAY(6);

    private final int order;

    DayOfWeek(int order) {
        this.order = order;
    }

    public int getOrder() {
        return order;
    }
}
