package hw.hw13.helpers;

import hw.hw13.entities.Family;
import hw.hw13.entities.people.Human;
import hw.hw13.entities.people.Man;
import hw.hw13.entities.people.Woman;
import hw.hw13.entities.pets.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import static java.util.Calendar.*;

public class DBRandomizer {
    private final String[] maleNames = {"Courtney","Theo","Eugene","Victor","Bruce", "Rafael","Anita", "Vincent", "Christian", "Ismail", "Andrew", "Verity","Jakob","Francis","Abby","Jose","Gary","Zack"};
    private final String[] femaleNames ={"Thea","Gabrielle","Charlie","Aimee","Jacqueline","Adele","Marie","Amirah","Amie","Beth","Tiana","Orla","Harris","Jessie","Hollie","Rachel","Faith","Rose","Mary","Linda(Lindy)"};
    private final String[] lastNames = {"Mccarthy","Crawford","O'Brien","Burke","Berry","Holmes","Edwards","Mcbride","Snyder","Perez","Fleming","Abbott","Kelley","Elliott","Walton","Todd","Hubbard","Rivera","Johnston"};
    private final String[] petNames ={"Cream","Roach","Kimbo","Twist","Rave","Guinness","Tension","Chapo","Perro","Mercury","Ninja","Organic","Patches","Dream","Toffee","Cora","Cookie","Phoenix","Rusty","Sweet","Pea"};
    private final Calendar now = Calendar.getInstance();
    private final int quantity = 1000;

    private int randomWithinRange(int min, int max) {
        Random r = new Random();
        int val = r.nextInt(max - min);
        return min + val;
    }

    private String randomString (String[] arr) {
        return arr[randomWithinRange(0, arr.length)];
    }

    private int getScaledProbabilityValue(){
        int i = randomWithinRange(0, 100);
        if (i <= 25) {
            return 0;
        }
        if (i <= 60) {
            return 1;
        }
        if (i <= 80) {
            return 2;
        }
        if (i <= 95) {
            return 3;
        }
        if (i <= 98) {
            return 4;
        }
        if (i <= 100) {
            return 5;
        }
        throw new RuntimeException("Unexpected random value");
    }

    private String generateBirthDate(int minAge, int maxAge) {
        Calendar birthDate = Calendar.getInstance();
        birthDate.set(randomWithinRange(now.get(YEAR)-maxAge, now.get(YEAR)-minAge), randomWithinRange(JANUARY, DECEMBER), 0);
        birthDate.set(birthDate.get(YEAR), birthDate.get(MONTH), randomWithinRange(0, birthDate.getActualMaximum(DAY_OF_MONTH)));
        return new DateConverter(birthDate.getTimeInMillis()).toString();
    }

    private Human generateFather(){
        return new Man(randomString(maleNames), randomString(lastNames), generateBirthDate(18,115), randomWithinRange(0,100));
    }

    private Human generateMother(Human father){
        String lastName;
        if (randomWithinRange(0,100)<70){
            lastName = father.getSurname();
        } else lastName = randomString(lastNames);

        int minAge = father.getFullYears()-15 < 18 ? 18 : father.getFullYears()-15;

        return new Woman(randomString(femaleNames), lastName, generateBirthDate(minAge, father.getFullYears()+15), randomWithinRange(0,100));
    }
    
    private Human generateChild(Family family) {
        int fatherAge = family.getFather().getFullYears();
        int motherAge = family.getMother().getFullYears();
        int maxAge = Math.min(fatherAge, motherAge) - 17;
        if (randomWithinRange(0,100) > 50) {
            return new Woman(randomString(femaleNames), family.getFather().getSurname(), generateBirthDate(0, maxAge), randomWithinRange(0,100));
        } else return new Man(randomString(maleNames), family.getFather().getSurname(), generateBirthDate(0, maxAge), randomWithinRange(0,100));
    }

    private Pet generatePet() {
        String[] habits = {"Stares at master", "Runs in circles", "Sleeps during sunny day", "Chills", "Does nothing", "Searching food on the ground", "Plays with tail"};
        Enum[] pets = {Species.DOG, Species.CAT, Species.ROBOCAT, Species.FISH};
        Pet pet = null;
        switch (randomWithinRange(0, pets.length)){
            case 0:
                pet = new Dog(randomString(petNames), randomWithinRange(0,10), randomWithinRange(0,75), habits[randomWithinRange(0, habits.length)]);
                break;
            case 1:
                pet = new DomesticCat(randomString(petNames), randomWithinRange(0,10), randomWithinRange(35,101), habits[randomWithinRange(0, habits.length)]);
                break;
            case 2:
                pet = new RoboCat(randomString(petNames), randomWithinRange(0,10), randomWithinRange(0,101), habits[randomWithinRange(0, habits.length)]);
                break;
            case 3:
                pet = new Fish(randomString(petNames), randomWithinRange(0,10), habits[randomWithinRange(0, habits.length)]);
                break;
        }

        if (pet != null) {
            for (int i = 0; i < randomWithinRange(0, 3); i++) {
                pet.addHabits(habits[randomWithinRange(0,habits.length)]);
            }
            return pet;
        } else throw new RuntimeException("Unexpected random value");

    }

    public List<Family> getDB() {
        ArrayList<Family> result = new ArrayList<>();
        for (int i = 0; i < quantity; i++) {
            Human father = generateFather();
            Family family = new Family(father, generateMother(father));
            int childrenToAdd = getScaledProbabilityValue();
            while (childrenToAdd > 0) {
                family.addChild(generateChild(family));
                --childrenToAdd;
            }
            int petsToAdd = getScaledProbabilityValue();
            while (petsToAdd > 0){
                family.addPet(generatePet());
                --petsToAdd;
            }
            result.add(family);
        }
        return result;
    }
}
