package hw.hw13.helpers;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateConverter {
    private final static SimpleDateFormat template = new SimpleDateFormat("dd/MM/yyyy");
    private Date date;

    private static Date parsedDate(String date) {
        try {
            return template.parse(date);
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format("Invalid date format %s, while expected %s", date, template.toPattern()));
        }
    }

    public DateConverter(String date) {
        this(parsedDate(date));
    }

    public DateConverter(long time) {
        this(new Date(time));
    }

    public DateConverter(Date date) {
        this.date = date;
    }

    public long getTime() {
        return date.getTime();
    }

    @Override
    public String toString(){
        if (date.getTime() != 0) {
            return template.format(date);
        } else return "not specified";
    }
}
