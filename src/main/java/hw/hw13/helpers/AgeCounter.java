package hw.hw13.helpers;

import java.util.Calendar;

import static java.util.Calendar.*;

public class AgeCounter {
    private Calendar birth;
    private Calendar now;
    private Calendar thisYBday;

    public AgeCounter(long birthDate) {
        this.birth = Calendar.getInstance();
        birth.setTimeInMillis(birthDate);
        this.now = Calendar.getInstance();
        if (birthDate == 0 | now.before(birth) | now.equals(birth)) throw new IllegalArgumentException("Invalid birth date");
        this.thisYBday = Calendar.getInstance();
        thisYBday.set(now.get(YEAR), birth.get(MONTH), birth.get(DAY_OF_MONTH));
    }


    public String describeAge(){
        int years;
        int months;
        int days;
        years = now.get(YEAR) - birth.get(YEAR);
        months = now.get(MONTH) - birth.get(MONTH);
        days = now.get(DAY_OF_MONTH) - birth.get(DAY_OF_MONTH);
        if (days < 0) {
            days = now.get(DAY_OF_MONTH) + thisYBday.getActualMaximum(DAY_OF_MONTH) - thisYBday.get(DAY_OF_MONTH);
            months -= 1;
        }
        if (months < 0) {
            years -= 1;
            months += 12;
        }

        return String.format("Years: %d, months: %d, days: %d", years, months, days);
    }

    public int getFullYears(){
        if (now.before(thisYBday)) {
            return now.get(YEAR) - birth.get(YEAR) - 1;
        } else return now.get(YEAR) - birth.get(YEAR);
    }

}
