package hw.hw13.service;

import hw.hw13.entities.Family;
import hw.hw13.dao.CollectionFamilyDAO;
import hw.hw13.dao.FamilyDAO;
import hw.hw13.entities.people.Human;
import hw.hw13.entities.pets.Pet;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import java.util.stream.Collectors;

public class FamilyService {
    FamilyDAO dao = new CollectionFamilyDAO();

    private void printFamiliesList(List<Family> list){
        List<Family> origin = dao.getAllFamilies();
        list.forEach(family -> {
            System.out.printf("%d ", origin.indexOf(family)+1);
            System.out.println(family.prettyFormat());
        });

    }
    public List<Family> getAllFamilies(){
        return dao.getAllFamilies();
    }

    public void displayAllFamilies(){
        int index = 0;
        for (Family fam : dao.getAllFamilies()) {
            System.out.printf("%d ", ++index);
            System.out.println(fam.prettyFormat());
        }
    }

    public List<Family> getFamiliesBiggerThan(int count){
        List <Family> result =  dao.getAllFamilies()
                .stream()
                .filter(family -> family.countFamily() > count)
                .collect(Collectors.toList());

        printFamiliesList(result);
        return result;
    }

    public List<Family> getFamiliesLessThan(int count){
        List<Family> result = dao.getAllFamilies()
                .stream()
                .filter(family -> family.countFamily() < count)
                .collect(Collectors.toList());

        printFamiliesList(result);
        return result;
    }

    public int countFamiliesWithMemberNumber(int count) {
        return (int) dao.getAllFamilies()
                .stream()
                .filter(family -> family.countFamily() == count)
                .count();
    }

    public void createNewFamily(Human partner1, Human partner2){
        if (partner1 != null & partner2 != null) {
            dao.saveFamily(new Family(partner1, partner2));
        } else throw new IllegalArgumentException("Invalid arguments, null is not accepted");
    }

    public boolean deleteFamilyByIndex(int index) {
        if (index >= 0) {
            return dao.deleteFamily(index);
        } else return false;
    }

    public boolean deleteFamily(Family family) {
        if (family != null) {
            return dao.deleteFamily(family);
        } else return false;
    }

    public Family bornChild(Family family, String girlName, String boyName){
        if (family != null & girlName != null & boyName != null) {
            family.bornChild(girlName, boyName);
            dao.saveFamily(family);
            return family;
        } else throw new IllegalArgumentException("Invalid argumetns: null is not accepted");
    }

    public Family adoptChild(Family family, Human child) {
        if (family != null & child != null) {
            family.addChild(child);
            dao.saveFamily(family);
            return family;
        } else throw new IllegalArgumentException("Invalid argumetns: null is not accepted");
    }

    public void deleteAllChildrenOlderThan(int age) {
        if (age >= 18) {
                dao.getAllFamilies()
                        .stream()
                        .forEach(family -> {
                            new ArrayList<>(family.getChildren()).forEach(child->{
                                if (child.getFullYears() > age) {
                                    family.deleteChild(child);
                                }
                            });
                            dao.saveFamily(family);
                        });
        } else if(age <= 0){
            throw new IllegalArgumentException("Invalid age");
        } else throw new IllegalArgumentException("Underaged children can't be removed from families");
    }

    public int count(){
        return dao.getAllFamilies().size();
    }

    public Family getFamilyByIndex(int index) {
        return dao.getFamilyByIndex(index);
    }

    public Set<Pet> getPets(int index) {
        Family family = dao.getFamilyByIndex(index);
        if (family != null) {
            return family.getAllPets();
        } else throw new IllegalArgumentException("Invalid argumetns: null is not accepted");

    }

    public void addPet(int index, Pet pet) {
        Family family = dao.getFamilyByIndex(index);
        if (family == null) {
            throw new IllegalArgumentException("Error while getting family by index");
        }
        if (pet != null) {
            family.addPet(pet);
        } else throw new IllegalArgumentException("Invalid argument: null is not accepted");
    }

    public void generateRandomData(){
        dao.generateRandomData();
    }

    public String save(){
        return dao.save();
    }

    public void loadData() {
        dao.loadData();
    }
}
