package hw.hw13.controllers;

import hw.hw13.entities.Family;
import hw.hw13.entities.people.Human;
import hw.hw13.entities.pets.Pet;
import hw.hw13.service.FamilyService;

import java.util.List;
import java.util.Set;

public class FamilyController {
    private FamilyService service = new FamilyService();

    public void generateRandomData() {
        service.generateRandomData();
    }

    public List<Family> getAllFamilies(){
        return service.getAllFamilies();
    }

    public void displayAllFamilies(){
        service.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int count){
        return service.getFamiliesBiggerThan(count);
    }

    public List<Family> getFamiliesLessThan(int count){
        return service.getFamiliesLessThan(count);
    }

    public int countFamiliesWithMemberNumber(int count) {
        return service.countFamiliesWithMemberNumber(count);
    }

    public void createNewFamily(Human partner1, Human partner2){
        service.createNewFamily(partner1, partner2);
    }

    public boolean deleteFamilyByIndex(int index) {
        return service.deleteFamilyByIndex(index);
    }

    public boolean deleteFamily(Family family) {
        return service.deleteFamily(family);
    }

    public Family bornChild(Family family, String girlName, String boyName){
        return service.bornChild(family, girlName, boyName);
    }

    public Family adoptChild(Family family, Human child) {
        return service.adoptChild(family, child);
    }

    public void deleteAllChildrenOlderThan(int age) {
        service.deleteAllChildrenOlderThan(age);
    }

    public int count(){
        return service.count();
    }

    public Family getFamilyByIndex(int index) {
        return service.getFamilyByIndex(index);
    }

    public Set<Pet> getPets(int index) {
        return service.getPets(index);
    }

    public void addPet(int index, Pet pet) {
        service.addPet(index, pet);
    }

    public String save(){
        return service.save();
    }

    public void loadData(){
        service.loadData();
    }

}
