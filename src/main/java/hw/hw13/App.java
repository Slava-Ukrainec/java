package hw.hw13;

import hw.hw13.entities.Family;
import hw.hw13.entities.people.Human;
import hw.hw13.entities.people.Man;
import hw.hw13.entities.people.Woman;
import hw.hw13.exceptions.ExitException;
import hw.hw13.exceptions.FamilyOverflowException;
import hw.hw13.logger.Logger;
import hw.hw13.ui.ConsoleManager;
import hw.hw13.controllers.FamilyController;
import hw.hw13.ui.IntValidator;
import hw.hw13.ui.StrValidator;

import java.util.ArrayList;
import java.util.LinkedList;

public class App {
    private ConsoleManager ui = new ConsoleManager();
    private FamilyController fc = new FamilyController();
    private Logger logger = Logger.getInstance();

    public void start(){
        logger.info("application started");
        for (;;){
            scenarioMainMenu();
        }
    }

    public void scenarioMainMenu(){
        logger.info("running main menu");
        try {
            switch (ui.getMainMenuChoice()) {
                case 1:
                    scenarioGenerateData();
                    break;
                case 2:
                    scenarioShowAllFamilies();
                    break;
                case 3:
                    scenarioShowFamiliesBiggerThan();
                    break;
                case 4:
                    scenarioShowFamiliesLessThan();
                    break;
                case 5:
                    scenarioCountFamiliesOfSize();
                    break;
                case 6:
                    scenarioCreateNewFamily();
                    break;
                case 7:
                    scenarioDeleteFamily();
                    break;
                case 8:
                    scenarioEditFamily();
                    break;
                case 9:
                    scenarioDeleteGrownUpKids();
                    break;
                case 10:
                    scenarioLoadData();
                    break;
                case 11:
                    scenarioSave();
                    break;
                default:
                    throw new RuntimeException("Unexpected main menu choice");
            }
            ui.pause();
        } catch (ExitException e){
            scenarioExit();

        } catch (Exception e) {
            logger.error(e);
            ui.showErrMsg(e);
        }

    }

    public void scenarioGenerateData(){
        fc.generateRandomData();
    }

    public void scenarioShowAllFamilies(){
        fc.displayAllFamilies();
    }

    public void scenarioShowFamiliesBiggerThan(){
        fc.getFamiliesBiggerThan(ui.getIntValue("Define benchmark family size"));

    }

    public void scenarioShowFamiliesLessThan(){
        fc.getFamiliesLessThan(ui.getIntValue("Define benchmark family size"));
    }

    public void scenarioCountFamiliesOfSize(){
        ui.showMessage(String.valueOf(fc.countFamiliesWithMemberNumber(ui.getIntValue("Define family size"))));
    }

    public void scenarioCreateNewFamily(){
        Human mother = createWoman("mother");
        Human father = createMan("father");
        fc.createNewFamily(father, mother);
        ui.showMessage("New " + father.getSurname() + " family was created");
    }


    private Human createMan(String appeal) {
        LinkedList<String> sData = ui.getPersonalData(appeal);
        int iq = ui.getIntValue(new IntValidator().withinRange(1,100), "Specify " + appeal + " iq");
        return new Man(sData.poll(),sData.poll(), sData.poll(), iq);
    }

    private Human createWoman(String appeal) {
        LinkedList<String> sData = ui.getPersonalData(appeal);
        int iq = ui.getIntValue(new IntValidator().withinRange(1,100), "Specify " + appeal + " iq");
        return new Woman(sData.poll(),sData.poll(), sData.poll(), iq);
    }


    public void scenarioDeleteFamily(){
        fc.deleteFamilyByIndex(ui.requestCollectionIndex(fc.getAllFamilies().size(), "Define family index to delete"));

        ui.showMessage("Family was deleted");
    }

    private void scenarioEditFamily(){
        switch (ui.getMenuChoice(new ArrayList<String>(){{add("1. Born child"); add("2. Adopt child"); add("3. Go to main menu");}})){
            case 1:
                scenarioBornChild();
                break;
            case 2:
                scenarioAdoptChild();
                break;
            case 3:
                break;
        }
    }
    
    private int getIdForFamilyExtension(){
        int id = ui.requestCollectionIndex(fc.getAllFamilies().size(), "Define index of family you want to extend");
        if (fc.getFamilyByIndex(id).getChildren().size() <= Family.CHILDREN_LIMIT) {
            return id;
        } else throw new FamilyOverflowException();
    }

    public void scenarioBornChild(){
        int id = getIdForFamilyExtension();
        String girlName = ui.getStringValue(new StrValidator().notEmpty(), "Put name if it's a girl");
        String boyName = ui.getStringValue(new StrValidator().notEmpty(), "Put name if it's a boy");
        fc.bornChild(fc.getFamilyByIndex(id), girlName, boyName);

        ui.showMessage("A new baby was born");
    }

    public void scenarioAdoptChild(){
        ui.showMessage("\nChoose action:");
        int id = getIdForFamilyExtension();
        String gender = ui.getStringValue(new StrValidator().fromOptions("male","female"), "Specify gender of adopted child");
        if (gender.toLowerCase().equals("male")){
            fc.adoptChild(fc.getFamilyByIndex(id), createMan("boy"));
        } else if (gender.toLowerCase().equals("female")){
            fc.adoptChild(fc.getFamilyByIndex(id), createWoman("girl"));
        } else throw new RuntimeException("Unexpected gender value");
    }
    public void scenarioDeleteGrownUpKids(){
        fc.deleteAllChildrenOlderThan(ui.getIntValue("Put age"));

        ui.showMessage("Grown up children were deleted");
    }

    public void scenarioLoadData(){
        fc.loadData();
        ui.showMessage("Data has been loaded");
    }
    public void scenarioSave(){
        String path = fc.save();
        ui.showMessage("Data has been saved to " + path);
    }

    public void scenarioExit(){
        System.out.println("Saving data before exit");
        fc.save();
        logger.info("closing application");
        System.exit(0);
    }
}
