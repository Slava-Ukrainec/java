package hw.hw13.logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {
    private static Logger instance = new Logger();
    private static final File file = new File("./storage", "application.log");
    public static Logger getInstance() {
        return instance;
    }

    private SimpleDateFormat template = new SimpleDateFormat("dd/MM/yyyy HH:mm");

    private Logger() {
    }

    static {
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
        } catch (IOException e) {
            throw new RuntimeException("Unable to get logger file");
        }
    }

    public void info(String msg){
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(file, true));
            out.write(formatLogData(LogTypes.DEBUG, msg));
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Error while logging file");
        }
    }

    public void error(Exception e) {
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(file, true));
            out.write(formatLogData(LogTypes.ERROR, e.getMessage()));
            out.close();
        } catch (IOException ex) {
            ex.printStackTrace();
            throw new RuntimeException("Error while logging file");
        }
    }

    public String formatLogData(Enum<LogTypes> type, String message) {
        return template.format(new Date()) + String.format("[%s]: ", type.name()) + message + "\n";
    }

}
