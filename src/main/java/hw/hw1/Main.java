package hw.hw1;

import java.util.Random;
import java.util.Scanner;

public class Main {
    private static Scanner in = new Scanner(System.in);
    private static String name;
    private static int targetNum;
    private static String[][] datesArr = {
            {"1970", "1939", "1991", "1826","1914", "1986", "1815"},
            {
                    "When did internet begin",
                    "When did the World War II begin",
                    "When independence of Ukraine was proclaimed",
                    "When Taras Shevchenko was born",
                    "When did World War I begin",
                    "When did Chornobyl catastrophe happen",
                    "When did Vaterloo battle happen",
            }
    };
    private static int input;
    private static int[] guessesArr = new int[99];
    private static int guessCounter = 0;
    private static boolean stopFlag = false;

    public static void main(String[] args) {
        getUserName();
        startGame();
        if (!stopFlag) {
            play();
        }
    }

    private static int selectGameVersion() {
        System.out.println("Hi, " + name + "\nSelect game version you want to play\n" +
                "1: guess random number from 0 to 100\n" +
                "2: check your history knowledge\n" +
                "print '1' or '2'");
        for (; ; ) {
            getIntInput();
            if (input != 1 && input != 2) {
                System.out.println("Please select option 1 or option 2");
            } else {
                return input;
            }
        }
    }

    private static int genRandom(int max) {
        Random generate = new Random();
        return generate.nextInt(max);
    }

    private static void pickHistoryDate() {
        int eventIndex = genRandom(datesArr[0].length);
        targetNum = Integer.parseInt(datesArr[0][eventIndex]);
        System.out.println(datesArr[1][eventIndex]);
    }

    private static void startGame() {
        int version = selectGameVersion();
        System.out.println("Let the game begin!\n" +
                "Print 'exit' any time you want to stop\n");
        if (version == 2) pickHistoryDate();
        else targetNum = genRandom(101);

//        System.out.println("Guess the number (SPOILER ALERT: it's " + targetNum + ")");
    }

    private static void getUserName() {
        System.out.println("Hi, what is your name");
        name = in.nextLine();
    }

    private static void play() {
        for (; ; ) {
            if (guessCounter == 0)
                System.out.println("What is your first guess?");
            getIntInput();
            if (stopFlag) {
                break;
            }
            saveGuess(input);
            if (input < targetNum) {
                System.out.println("Your number is too small. Please, try again.");
            } else if (input > targetNum) {
                System.out.println("Your number is too big. Please, try again.");
            } else {
                finishGame();
                break;
            }
        }
    }

    private static void finishGame() {
        System.out.println("Congratulations, " + name + "!");
        System.out.println("Your guesses:");
        showArrayPart(sortArrayMaxToMin(guessesArr), guessCounter);
    }

    private static void saveGuess(int num) {
        guessesArr[guessCounter++] = num;

    }

    private static void showArrayPart(int[] arr, int lastItemPosition) {
        for (int i = 0; i < lastItemPosition; i++) {
            String separator = i == lastItemPosition - 1 ? "." : ", ";
            System.out.print(arr[i] + separator);
        }
        System.out.print("\n");
    }

    private static int[] sortArrayMaxToMin(int[] arr) {
        for (int i = arr.length - 1; i > 0; i--) {
            for (int item = 0; item < i; item++) {
                if (arr[item] < arr[item + 1]) {
                    int tmp = arr[item];
                    arr[item] = arr[item + 1];
                    arr[item + 1] = tmp;
                }
            }
        }
        return arr;
    }

    private static boolean isInteger(String s) {
        Scanner sc = new Scanner(s.trim());
        return sc.hasNextInt();
    }

    private static void getIntInput() {
        for (; ; ) {
            String userInput = in.nextLine();
            if (userInput.equals("exit")) {
                stopFlag = true;
                input = -999;
                break;
            }
            if (!isInteger(userInput)) {
                System.out.println("It's not a number. Please, try again");
            } else {
                input = Integer.parseInt(userInput);
                break;
            }
        }
    }
}
