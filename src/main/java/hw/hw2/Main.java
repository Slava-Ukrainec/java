package hw.hw2;

import java.util.Random;
import java.util.Scanner;

public class Main {
    private static Scanner in = new Scanner(System.in);
    private static String[][] playingField;
    private static int[][] targetArea = new int[9][2];
    private static int[][] scoredPoints = new int[9][2];
    private static int scoredCounter = 0;

    public static void main(String[] args) {
        generateField(6);
        setTarget();
        System.out.println("All set. Get ready to rumble!");
        play();
    }

    private static void generateField(int size) {
        playingField = new String[size][size];
        for (int i = 0; i < size; i++) {
            playingField[0][i] = String.valueOf(i);
            playingField[i][0] = String.valueOf(i);
        }
        for (int r = 1; r < size; r++) {
            for (int c = 1; c < size; c++) {
                playingField[r][c] = "-";
            }
        }
    }

    private static void setTarget () {
        int[] targetCenter = setTargetCenter(playingField.length);
        createTargetArea(targetCenter);
    }


    private static int[] setTargetCenter(int fieldSize) {
        Random generate = new Random();
        int[] target = new int[2];
        /* as far as targetArea must be 3x3, target center mustn't be near field border,
         thus Random generator re-generate values <=1 and restricted from generating
         value that equals the number of last row/column
         */
        do {
            if (target[0] <= 1) {
                target[0] = generate.nextInt(fieldSize-1);
            }
            if (target[1] <= 1) {
                target[1] = generate.nextInt(fieldSize-1);
            }
        }
        while (target[0] <= 1 || target[1] <= 1);
        return target;
    }

    private static void createTargetArea (int[] targetCenter) {
        System.out.println("***FOR EASIER CHECK*** \n ***SPOILER ALERT*** \n TARGET CENTER: { " + targetCenter[0] + ", " + targetCenter[1] + " }");
        int targetCounter = 0;
        for (int col = -1; col < 2; col++) {
            for (int row = -1; row < 2; row++) {
                int[] targetPoint = {targetCenter[0]+col, targetCenter[1]+row};
                targetArea[targetCounter++] = targetPoint;
            }
        }
    }


    private static void printField() {
        for (int i = 0; i < playingField.length; i++) {
            for (String val : playingField[i]) {
                System.out.print(" " + val + " |");
            }
            System.out.print("\n");
        }
    }

    private static void play() {
        for (; ; ) {
            printField();
            int[] shot = getShotCoordinates();

            if(!ifScored(shot)){
                putShotMark(shot);
                System.out.println("You've missed, try again");
            } else {
                handleScore(shot);
                if (ifWon()) {
                    System.out.println("You have won!");
                    printField();
                    break;
                }
            }
        }
    }

    private static int[] getShotCoordinates() {
        int[] coordinates = new int[2];
        String[] descriptions = {"row", "column"};
        for (int i = 0; i<coordinates.length; i++) {
            System.out.println("What " + descriptions[i] + " you are going to shoot");
            coordinates[i] = getIntInput(playingField.length);
        }
        System.out.println("SHOT COORDINATES: {" + coordinates[0] + ", " + coordinates[1] + "}");
        return coordinates;
    }

    private static int getIntInput(int bound) {
        for (; ; ) {
            String userInput = in.nextLine();
            if (isInteger(userInput)) {
                int intInput = Integer.parseInt(userInput);
                if (isWithinField(intInput, bound)) {
                    System.out.println("Number is outside playing field. Please, try again");
                } else { return intInput; }
            } else { System.out.println("It's not a number. Please, try again"); }
        }
    }

    private static boolean isInteger(String s) {
        Scanner sc = new Scanner(s.trim());
        return sc.hasNextInt();
    }

    private static boolean isWithinField (int input, int bound) {
        return input >= bound || input < 1;
    }



    private static void putShotMark(int[] coordinates) {
        playingField[coordinates[0]][coordinates[1]] = "*";
    }

    private static void putScoreMark(int[] coordinates) {
        playingField[coordinates[0]][coordinates[1]] = "X";
    }

    private static boolean ifScored (int[] coordinates) {
        for (int[] targetPoint : targetArea) {
            if (targetPoint[0] == coordinates[0] && targetPoint[1] == coordinates[1]) {
                return true;
            }
        }
        return false;
    }

    private static void handleScore (int[] coordinates) {
        if(ifScoreIsNew(coordinates)) {
            scoredPoints[scoredCounter++] = coordinates;
            putScoreMark(coordinates);
            System.out.println("You've hit the target");
        }
        else {
            System.out.println("You've already hit that point");
        }
    }

    private static boolean ifScoreIsNew (int[] coordinates) {
        boolean newScore = true;
        for (int[] alreadyScored : scoredPoints) {
            if (alreadyScored[0] == coordinates[0] && alreadyScored[1] == coordinates[1]) {
                newScore = false;
                break;
            }
        }
        return newScore;
    }

    private static boolean ifWon () {
        return scoredCounter == 9;
    }

}
