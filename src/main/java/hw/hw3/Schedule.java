package hw.hw3;

import java.util.Scanner;
import java.util.StringJoiner;


public class Schedule {
    private static Scanner in = new Scanner(System.in);
    private static String[] weekDays = new String[]{
            "monday",
            "tuesday",
            "wednesday",
            "thursday",
            "friday",
            "saturday",
            "sunday"
    };

    private String[][] schedule = new String[7][2];
    private boolean exitFlag = false;

    public String[][] getSchedule(){
        return this.schedule;
    }
    private void maintainReschedule(int day) {
        System.out.println(String.format("Please, input new tasks for %s", weekDays[day]));
        String tasks = in.nextLine();
        this.schedule[day][1] = tasks;
    }

    private int selectedWeekDay(String day) {
        int result = -1;
        for (int i = 0; i<weekDays.length ; i++) {
            if (weekDays[i].equals(day)) {
                result = i;
                break;
            }
        }
        return result;
    }

    private void generateTaskReply (String day, String tasks) {
        System.out.println("Your tasks for " + day + ": " + tasks);
    }

    private void maintainCommand (String input) {
        String[][] schedule = this.getSchedule();
        String[] command = input.toLowerCase().trim().split(" ");
        switch (command[0]) {
            case  ("monday"):
                generateTaskReply(schedule[0][0], schedule[0][1]);
                break;
            case  ("tuesday"):
                generateTaskReply(schedule[1][0], schedule[1][1]);
                break;
            case  ("wednesday"):
                generateTaskReply(schedule[2][0], schedule[2][1]);
                break;
            case  ("thursday"):
                generateTaskReply(schedule[3][0], schedule[3][1]);
                break;
            case  ("friday"):
                generateTaskReply(schedule[4][0], schedule[4][1]);
                break;
            case  ("saturday"):
                generateTaskReply(schedule[5][0], schedule[5][1]);
                break;
            case  ("sunday"):
                generateTaskReply(schedule[6][0], schedule[6][1]);
                break;
            case ("change"):
                int day = selectedWeekDay(command[1]);
                if (day != -1) {
                    maintainReschedule(day);
                } else {
                    System.out.println("Incorrect day. Please, select day once again");
                }
                break;
            case  ("exit"):
                exitFlag = true;
                break;
            default:
                System.out.println("Sorry, I don't understand you, please try again.");
                break;
        }
    }

    public String toString() {
        StringJoiner sj = new StringJoiner(",","[","]");
        for (String[] day:schedule
             ) {
            sj.add(String.format("[%s , %s]", day[0], day[1]));
        }
        return sj.toString();
    }

    private void genSchedule() {
        for (int day = 0; day < this.schedule.length; day++) {
            this.schedule[day][0] = weekDays[day];
            this.schedule[day][1] = "Task for " + weekDays[day] + " wasn't specified yet";
        }
    }

    public Schedule(){
        this.genSchedule();
    }

    public void run(){
        this.exitFlag = false;
        for (;;){
            System.out.println("Please, input the day of the week:");
            String input = in.nextLine();
            maintainCommand(input);
            if (this.exitFlag) {break;}
        }
    }
    public static void main(String[] args) {
    }

}
