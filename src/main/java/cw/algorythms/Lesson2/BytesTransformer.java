package cw.algorythms.Lesson2;

public class BytesTransformer {

    public static int transform(String num) {
        String[] str = num.split("");
        int mult = 1;
        int result = 0;
        for (int i = str.length-1; i >= 0 ; i--) {
            result += Integer.parseInt(str[i]) * mult;
            mult *= 2;
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(transform("1000"));
    }
}
