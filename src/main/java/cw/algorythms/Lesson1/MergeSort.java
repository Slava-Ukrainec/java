package cw.algorythms.Lesson1;

public class MergeSort {
    int [] mergeArrays (int[] arr1, int[] arr2) {
        int l1 = arr1.length;
        int l2 = arr2.length;
        int l3 = l1 + l2;

        int[] result = new int[l3];

        int i1 = 0;
        int i2 = 0;
        int ri = 0;
        while (i1 < l1 && i2 < l2) {
            if (arr1[i1] < arr2[i2]) {
                result[ri] = arr1[i1++];
            } else {
                result [ri] = arr2[i2++];
            }
            ri++;
        }

        while (i1 < l1) {
            result[ri++]=arr1[i1++];
        }

        while (i2 < l2) {
            result[ri++]=arr2[i2++];
        }


        return result;
    }
}
