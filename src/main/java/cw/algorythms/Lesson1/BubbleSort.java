package cw.algorythms.Lesson1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class BubbleSort {

    public static int randomRange(int min, int max) {
        return min + new Random().nextInt(max-min);
    }
    public static void main(String[] args) {
        int[] data = new int[10];
        for (int i = 0; i < data.length ; i++) {
            data[i] = randomRange(1, 200);
        }

        for (int i = 0; i < data.length ; i++) {
            for (int j = i; j < data.length; j++) {
                if (data[j]<data[i]) {
                    int t = data[i];
                    data[i] = data[j];
                    data[j]= t;
                }
            }
        }
        System.out.println(Arrays.toString(data));
    }
}
