package cw.javaBasic.Lesson12;

import java.util.Iterator;

public class Days implements Iterable<String> {
    private final String[] days = {"Mon","Tue","Wed","Thu","Fri","Sat","Sun"};

    @Override
    public Iterator<String> iterator() {
        return new Iterator<String>() {
            int position = 0;

            @Override
            public boolean hasNext() {
                return position < days.length;
            }

            @Override
            public String next() {
                return days[position++];
            }
        };
    }
}
