package cw.javaBasic.Lesson12;

public interface Logger {
    void log (String s);
    void debug (String s);
    void trace (String s);
}
