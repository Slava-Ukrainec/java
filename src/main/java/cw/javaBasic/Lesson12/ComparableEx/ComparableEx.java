package cw.javaBasic.Lesson12.ComparableEx;

import cw.javaBasic.Lesson8.Interface.Human;

import java.util.ArrayList;
import java.util.Collections;

public class ComparableEx {

    public static void main(String[] args) {

        Human alex = new Human("Alex");
        Human masha = new Human("Masha");
        Human dima = new Human("Dima");
        ArrayList<Human> people = new ArrayList<>();
        people.add(alex);
        people.add(dima);
        people.add(masha);
        Collections.sort(people);
        System.out.println(people);
    }

}
