package cw.javaBasic.Lesson12;

import cw.javaBasic.Lesson12.Task.*;

import java.util.PriorityQueue;

public class PriorityQueueAF {
    public static void main(String[] args) {
        PriorityQueue<AbstractFigure> q = new PriorityQueue<>(new CompAFbySides());
        q.add(new Hexagon(10));
        q.add(new Triangle(20,20,20));
        q.add(new Square(15));

        while (!q.isEmpty()){
            System.out.println(q.poll());
        }
    }
}
