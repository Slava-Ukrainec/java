package cw.javaBasic.Lesson12;

import java.util.ArrayList;
import java.util.Iterator;

public class IterEx {
    public static void main1(String[] args) {
        int[] a = {1,2,3};
        for (int el: a) {
            System.out.println(el);
        }
    }

    public static void main2(String[] args) {
        ArrayList<Integer> a = new ArrayList<>();
        a.add(10);
        a.add(20);
        a.add(30);
        a.add(40);
        for (int el: a) {
            System.out.println(el);
        }
    }

    public static void main3(String[] args) {
        Days days = new Days();
        for (String day: days) {
            System.out.println(day);
        }
    }

    public static void main(String[] args) {
        Days days = new Days();
        Iterator<String> it = days.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }
    }
}
