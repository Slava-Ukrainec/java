package cw.javaBasic.Lesson12;

public class ConsoleLogger implements Logger {
    private final Level level;

    public ConsoleLogger(Level l) {
        this.level = l;
    }

    @Override
    public void log(String s) {
        if (level == Level.Log || level == Level.Debug || level == Level.Trace)
        System.out.printf("INFO:%s\n", s);
    }

    @Override
    public void debug(String s) {
        if (level == Level.Debug || level == Level.Trace)
        System.out.printf("DETAILED INFO:%s\n", s);
    }

    @Override
    public void trace(String s) {
        if (level == Level.Trace)
        System.out.printf("SUPERDETAILED INFO:%s\n", s);
    }
}
