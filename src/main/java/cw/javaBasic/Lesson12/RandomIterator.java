package cw.javaBasic.Lesson12;

import java.util.Iterator;

public class RandomIterator implements Iterable<Integer> {
    @Override
    public Iterator<Integer> iterator() {
        return new Iterator<Integer>() {

            int position;
            @Override
            public boolean hasNext() {
                return position < 10;
            }

            @Override
            public Integer next() {
                return (int)Math.floor(Math.random()*100);
            }
        };
    }
}
