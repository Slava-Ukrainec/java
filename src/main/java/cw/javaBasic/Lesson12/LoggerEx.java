package cw.javaBasic.Lesson12;

public class LoggerEx {

    public static void main(String[] args) {
        Logger console = new ConsoleLogger(Level.None);

        console.log("Level 1");
        console.debug("Level 2");
        console.trace("Level 3");
    }

}
