package cw.javaBasic.Lesson12.Task;

public class Triangle extends AbstractFigure {
    private final int side1Length;
    private final int side2Length;
    private final int side3Length;
    private final int sideLength;

    public Triangle(int side1Length, int side2Length, int side3Length) {
        super(3);
        this.side1Length = side1Length;
        this.side2Length = side2Length;
        this.side3Length = side3Length;
        this.sideLength = side1Length;
    }

    @Override
    public int getSquare() {
        double s1 = (double)side1Length;
        double s2 = (double)side2Length;
        double s3 = (double)side3Length;
        double p = (s1 + s2 + s3)/2;
        return (int)(Math.sqrt(p * (p-s1) * (p-s2) * (p-s3)));
    }
}
