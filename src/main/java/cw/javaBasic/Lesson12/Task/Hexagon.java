package cw.javaBasic.Lesson12.Task;

public class Hexagon extends AbstractFigure {
    private final int sideLength;
    public Hexagon(int sideLength) {
        super(6);
        this.sideLength = sideLength;
    }

    @Override
    public int getSquare() {
        return (int)((3 * Math.sqrt(3) * sideLength * sideLength)/2);
    }
}
