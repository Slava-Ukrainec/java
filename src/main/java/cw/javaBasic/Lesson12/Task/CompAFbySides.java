package cw.javaBasic.Lesson12.Task;

import java.util.Comparator;

public class CompAFbySides implements Comparator<AbstractFigure> {
    @Override
    public int compare(AbstractFigure o1, AbstractFigure o2) {
        return o1.getSides() - o2.getSides();
    }
}
