package cw.javaBasic.Lesson12.Task;

abstract public class AbstractFigure implements Comparable<AbstractFigure> {
    public int sides;

    public AbstractFigure(int sides) {
        this.sides = sides;
    }

    public int getSides() {
        return sides;
    }

    abstract public int getSquare();

    @Override
    public int compareTo(AbstractFigure o) {
        return this.getSquare() - o.getSquare();
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getSimpleName());
        sb.append("{");
        sb.append("Square");
        sb.append(this.getSquare());
        sb.append("}");
        return sb.toString();
    }
}
