package cw.javaBasic.Lesson12.Task;

import java.util.ArrayList;

public class FiguresComparison {
    public static void main(String[] args) {
        Circle circle = new Circle(4);
        Hexagon hexagon = new Hexagon(5);
        Square square = new Square(6);
        Triangle triangle = new Triangle(3, 3, 3);
        ArrayList<AbstractFigure> figures = new ArrayList<>();
        figures.add(circle);
        figures.add(hexagon);
        figures.add(square);
        figures.add(triangle);
        figures.sort(new CompAFbySides());
        figures.sort((o1, o2) -> o1.getSquare() - o2.getSquare());
        System.out.println(figures);
        System.out.println(figures);
    }
}
