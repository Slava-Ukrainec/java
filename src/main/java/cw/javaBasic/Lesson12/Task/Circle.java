package cw.javaBasic.Lesson12.Task;

public class Circle extends AbstractFigure {
    private final int radius;
    private final int sideLength = 0;

    public Circle(int radius) {
        super(0);
        this.radius = radius;
    }

    @Override
    public int getSquare() {
        return (int)(Math.PI * radius * radius);
    }
}
