package cw.javaBasic.Lesson12.Task;

public class Square extends AbstractFigure {
    private final int sideLength;

    public Square(int sideLength) {
        super(4);
        this.sideLength = sideLength;
    }

    @Override
    public int getSquare() {
        return sideLength * sideLength;
    }
}
