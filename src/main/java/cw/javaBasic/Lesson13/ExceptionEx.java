package cw.javaBasic.Lesson13;

public class ExceptionEx {

    static boolean check(String origin) {
        try {
            for (int i = 0; i <origin.length() ; i++) {
                if (origin.charAt(i) < '0' || origin.charAt(i)>'9') {
                return false;
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    static int smart_convert(String origin) {
        if (check(origin)) return Integer.parseInt(origin);
        throw new IllegalArgumentException("Can't transform");
    }

    public static void main(String[] args) {
        String s = "123x";
        if (check(s)) {
            System.out.println(Integer.parseInt(s));
        } else {
            System.out.println("Can't transform");
        }
        System.out.println(smart_convert(s));
    }
}
