package cw.javaBasic.Lesson13;

public class LoggerEx {
    static int add (int a, int b) {
        System.out.println("add - entrance");
        System.out.println("a=" +a);
        System.out.println("b=" +b);
        System.out.println("add - done");
        return a + b;
    }

    public static void main(String[] args) {
        System.out.println("calling add");
        System.out.println(add(1,2));
        System.out.println("calling add - done");
    }
}
