package cw.javaBasic.Lesson7;

import java.util.PriorityQueue;

public class PriorityQueueEx {
    public static void main(String[] args) {
        PriorityQueue q = new PriorityQueue();
        for (int i = 0; i < 100; i++) {
            q.add((int)(Math.random() * 100 +50));
        }
        while (!q.isEmpty()){
            System.out.print(q.poll()+":");
        }
    }
}
