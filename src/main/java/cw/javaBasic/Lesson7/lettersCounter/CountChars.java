package cw.javaBasic.Lesson7.lettersCounter;

import java.util.HashMap;

public class CountChars {
    public static void main(String[] args) {
        String origin = "This is the test string";
        CharCounter c = new CharCounter();
        for (int i =0; i<origin.length(); i++) {
            c.addChar(new CharData<>(origin.charAt(i),i));
        }

//        printMap(new HashMap<>(c.getWithCount()),"quantity");
//        printMap(new HashMap<>(c.get()),"entries");
        printMap(new HashMap<>(c.getWithCountAndPositions()),"Quantity and positions");
    }

    public static <T> void printMap(HashMap<Character,T> input, String kpi) {
        for (Character key: input.keySet()) {
            System.out.println(String.format("Character: '%s' - %s: ", key, kpi) + input.get(key));
        }
    }
}
