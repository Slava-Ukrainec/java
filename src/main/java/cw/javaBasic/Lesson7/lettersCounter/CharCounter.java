package cw.javaBasic.Lesson7.lettersCounter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CharCounter {
    private final Map<Character, ArrayList<Integer>> storage = new HashMap<>();
    public void addChar (CharData<Character, Integer> c){
        boolean exists = storage.containsKey(c.ch);
        ArrayList<Integer> positions;
        if (exists) {
            positions = storage.get(c.ch);
        } else {
            positions = new ArrayList<>();
        }
        positions.add(c.pos);
        storage.put(c.ch,positions);
    }
    public Map<Character,ArrayList<Integer>> get(){
        return storage;
    }

    public Map <Character,Integer> getWithCount(){
        Map <Character,Integer> result = new HashMap<>();
        for (Map.Entry<Character,ArrayList<Integer>> entry: storage.entrySet()){
            result.put(entry.getKey(),entry.getValue().size());
        }
        // MY METHOD THAT DOES THE SAME
//        for (Character key: storage.keySet()) {
//            result.put(key, storage.get(key).size());
//        }
        return result;
    }

    public Map <Character, Pair<Integer, ArrayList<Integer>>> getWithCountAndPositions(){
        Map <Character,Pair<Integer, ArrayList<Integer>>> result = new HashMap<>();
        storage.forEach((ch,list)->result.put(ch, new Pair<>(list.size(),list)));
        return result;
    }

// SAME METHOD AS getWithCount implemented with forEach
    public Map <Character,Integer> getWithCountForEach(){
        Map <Character,Integer> result = new HashMap<>();
        storage.forEach((ch,list)->result.put(ch,list.size()));
        return result;
    }

    public void print() {
        for (Character key: storage.keySet()) {
            System.out.println(String.format("Character: '%s' ; entries: ", key) + storage.get(key));
        }
    }


}
