package cw.javaBasic.Lesson7.lettersCounter;

public class Pair<A, B> {
    final A a;
    final B b;

    public Pair(A a, B b) {
        this.a = a;
        this.b = b;
    }

    public String toString() {
        return a + ", " + b;
    }
}
