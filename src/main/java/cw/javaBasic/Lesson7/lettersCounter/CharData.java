package cw.javaBasic.Lesson7.lettersCounter;

public class CharData<A, B> {
    final A ch;
    final B pos;

    CharData(A ch, B pos) {
        this.ch = ch;
        this.pos = pos;
    }

    public String toString() {
        return String.format("<char: %s, pos: %d>", this.ch, this.pos);
    }
}
