package cw.javaBasic.Lesson7;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class StringExercise {
    public static void main(String[] args) {
        ArrayList<Character> letters = new ArrayList<>();
        String origin = "text";
        System.out.println(getLetters(origin));
    }

    public static Map<Character, Integer> getLetters (String str) {
        Map<Character, Integer> result = new HashMap<Character,Integer>();
        for (int i =0 ; i<str.length(); i++) {
            result.put(str.charAt(i), 1);
        }
        return result;
    }

}
