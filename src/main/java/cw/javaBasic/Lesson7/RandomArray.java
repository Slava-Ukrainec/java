package cw.javaBasic.Lesson7;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class RandomArray {
    public static int length = 100;

    public static void main(String[] args) {
        ArrayList<Integer> digits = new ArrayList<>();
        for (int i = 0; i < length; i++) {
            digits.add((int)(Math.random() * 100 +50));
        }

        ArrayList<Integer> pairs = getAllPairs(digits);
        printArr(pairs);
        printMax(pairs,3);
    }

    public static void printArr(ArrayList<Integer> arr) {
        StringJoiner sj = new StringJoiner(":","<",">");
        arr.forEach(integer -> sj.add(String.valueOf(integer)));
        System.out.println(sj);
    }

    public static void printMax(ArrayList<Integer> arr, int n) {
        List<Integer> list = arr
                .stream()
                .sorted((o1, o2) -> o2-o1)
                .limit(n)
                .collect(Collectors.toList());
        ArrayList<Integer> toPrint = new ArrayList<>(list);
        printArr(toPrint);
    }

    public static ArrayList<Integer> getAllPairs (ArrayList<Integer> arr) {
        ArrayList<Integer> result = new ArrayList<>();
        for (Integer item: arr) {
            if (arr.indexOf(item) != arr.lastIndexOf(item)) {
                if (!result.contains(item)) {result.add(item);}
            }
        }
        return result;
    }

}
