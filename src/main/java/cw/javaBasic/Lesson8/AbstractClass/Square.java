package cw.javaBasic.Lesson8.AbstractClass;

public class Square extends AbstractFigure {
    private final Point pOne;
    private final Point pTwo;

    public Square(Point one, Point two) {
        super("Square");
        this.pOne = one;
        this.pTwo = two;
    }


    public void print(){
        System.out.println(this);
    }

    int square() {
        return Math.abs(pTwo.getX() - pOne.getX()) * Math.abs(pTwo.getY()-pOne.getY());
    }

    @Override
    public String toString() {
        return String.format("%s. pOne:%s, pTwo:%s", name, pOne, pTwo);
    }
}
