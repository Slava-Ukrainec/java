package cw.javaBasic.Lesson8.AbstractClass;

public abstract class AbstractFigure {
    protected final String name;

    protected  AbstractFigure(String name) {
        this.name = "*" + name + "*";
    }

    abstract void print();
    int square(){
        throw new IllegalArgumentException("AbstractFigure:square is not implemented yet");
    }
}
