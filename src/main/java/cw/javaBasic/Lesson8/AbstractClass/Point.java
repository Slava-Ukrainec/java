package cw.javaBasic.Lesson8.AbstractClass;

public class Point extends AbstractFigure {
    private final int x;
    private final int y;

    Point(int x, int y) {
        super("Point");
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void print(){
        System.out.println(this);
    }

    int square() {
        return 0;
    }

    @Override
    public String toString() {
        return String.format("%s coordinates: x:%d, y:%d",name, x, y);
    }
}
