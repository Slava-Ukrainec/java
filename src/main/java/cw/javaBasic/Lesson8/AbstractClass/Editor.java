package cw.javaBasic.Lesson8.AbstractClass;

public class Editor {
    public static void main(String[] args) {
        Point point = new Point(5, 6);
        Square square = new Square(new Point(1, 2), new Point(4, 5));

//        System.out.println(point);
//        System.out.println(square);
//        System.out.println(square.square());

        printFigure(square);
        square.print();
    }

    static void printFigure (AbstractFigure figure) {
        System.out.println(figure);
    }
}
