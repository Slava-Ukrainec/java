package cw.javaBasic.Lesson8.Interface;

public class Human implements Printable, Tellable, Thinkable, Comparable<Human> {
    public String name;

    public Human(String name) {
    this.name = name;
    }

    @Override
    public void think() {
        System.out.println("Who we are???");
    }

    @Override
    public void print() {
        System.out.println("Human printed");
    }

    @Override
    public void tell() {
        System.out.println("hi!");
    }

    @Override
    public int compareTo(Human another){
        return this.name.compareTo(another.name);
    }

    @Override
    public String toString() {
        return this.name;
    }
}
