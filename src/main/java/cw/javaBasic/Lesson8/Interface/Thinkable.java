package cw.javaBasic.Lesson8.Interface;

public interface Thinkable {
    void think();
}
