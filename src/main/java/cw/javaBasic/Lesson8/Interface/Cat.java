package cw.javaBasic.Lesson8.Interface;

public class Cat implements Thinkable, Tellable {
    @Override
    public void tell() {
        System.out.println("Meow");
    }

    @Override
    public void think() {
        System.out.println("looks anxiously");
    }
}
