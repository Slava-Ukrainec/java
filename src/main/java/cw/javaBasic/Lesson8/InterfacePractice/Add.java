package cw.javaBasic.Lesson8.InterfacePractice;

public class Add implements Operations {
    private final int a;
    private final int b;

    Add(int a, int b){
        this.a = a;
        this.b = b;
    }

    public int operation() {
        return a+b;
    }
}
