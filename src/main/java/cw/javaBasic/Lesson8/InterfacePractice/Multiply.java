package cw.javaBasic.Lesson8.InterfacePractice;

public class Multiply implements Operations {
    private final int a;
    private final int b;

    Multiply(int a, int b){
        this.a = a;
        this.b = b;
    }

    public int operation() {
        return a*b;
    }
}
