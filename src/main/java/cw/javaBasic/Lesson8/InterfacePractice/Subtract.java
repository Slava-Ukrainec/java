package cw.javaBasic.Lesson8.InterfacePractice;

public class Subtract implements Operations {
    private final int a;
    private final int b;

    Subtract(int a, int b){
        this.a = a;
        this.b = b;
    }

    public int operation() {
        return a-b;
    }
}
