package cw.javaBasic.Lesson8.InterfacePractice;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Operations> operations = new ArrayList<>();
        operations.add(new Add(5,5));
        operations.add(new Subtract(10,2));
        operations.add(new Multiply(4,3));
        operations.add(new Add(1,3));
        operations.add(new Divide(63,8));
        operations.add(new Divide(36,9));

        operations.forEach((item)-> System.out.println(item.operation()));
    }
}
