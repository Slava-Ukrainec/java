package cw.javaBasic.Lesson10;

import java.util.ArrayList;
import java.util.List;

public class NullExample {

    void print1(String s) {
        if (s != null) {
            System.out.println(s);
        }
    }

    void print(List<String> l) {
        l.forEach(System.out::println);
    }

    public static void main(String[] args) {
        NullExample x = new NullExample();
        x.print(new ArrayList<String>(){{add("Hello");}});
        x.print1(null);
    }
}
