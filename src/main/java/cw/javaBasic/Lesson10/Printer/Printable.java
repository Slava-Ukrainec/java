package cw.javaBasic.Lesson10.Printer;

public interface Printable {
    String format(String s);
    default void print(String s)  {
        System.out.println(format(s));
    }
}
