package cw.javaBasic.Lesson10.Printer;

public class PrintableApp {
    public static void print7(String s, Printable format) {
        format.print(s);
    }

    public static void main(String[] args) {
        Printable format = new PrintFormat3();

        String s = "Hello";

        format.print(s);
        format.print(s);
        format.print(s);
        format.print(s);
        format.print(s);

    }
}
