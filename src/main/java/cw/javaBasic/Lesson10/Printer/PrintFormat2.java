package cw.javaBasic.Lesson10.Printer;

public class PrintFormat2 implements Printable {
    public String format(String s) {
        StringBuilder sb = new StringBuilder();
//        sb.append("=================\n");
        sb.append(s);
//        sb.append("\n");
//        sb.append("=================\n");
        return sb.toString();
    }
    public void print(String s) {
        System.out.println(format(s));
    }
}
