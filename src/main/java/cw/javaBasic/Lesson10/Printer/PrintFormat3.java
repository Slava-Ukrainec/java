package cw.javaBasic.Lesson10.Printer;

import java.util.Collections;

public class PrintFormat3 implements Printable {

    public String format(String s) {
        StringBuilder sb = new StringBuilder();
        int length = s.length();
        String header = String.join("", Collections.nCopies(length+6, "="));
        return sb.append(header)
                .append(String.format("|  %s  |\n", s))
                .append(header)
                .toString();

    }

    public void print(String s) {
        System.out.println(format(s));
    }
}
