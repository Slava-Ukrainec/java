package cw.javaBasic.Lesson6;

import java.util.ArrayList;

public class ArrayListEx {

    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.get(0); // 1
        list.get(1); // 2
        list.size();
        list.contains(1); // true
/*
ArrayList
LinkedList
Queue
DeQueue
Stack
Set
Map
 */
    }
}
