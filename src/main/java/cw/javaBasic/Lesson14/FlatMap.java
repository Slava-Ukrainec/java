package cw.javaBasic.Lesson14;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FlatMap {
    public static void main(String[] args) {
        final int[][] nested = {{1,2,3},{4,5}}; // ArrayList<Int> {1,2,3,4,5}
        Stream<int[]> level1 = Arrays.stream(nested);
        Stream<Integer> level2 = level1.flatMap((Function<int[], Stream<Integer>>) ints -> Arrays.stream(ints).boxed());
        List<Integer> l = level2.collect(Collectors.toList());
        System.out.println(l);

//        level2.forEach(new Consumer<Integer>() {
//            @Override
//            public void accept(Integer integer) {
//                System.out.println(integer);
//            }
//        });

        ArrayList<Integer> arr = new ArrayList<>();
        arr.add(1);

    }
}
