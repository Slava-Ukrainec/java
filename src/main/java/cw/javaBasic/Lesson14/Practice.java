package cw.javaBasic.Lesson14;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Practice {

    public static void main1(String[] args) {
        List<Integer> div2 = new ArrayList<>();
        List<Integer> div3 = new ArrayList<>();
        PriorityQueue<Integer> other = new PriorityQueue<>();

        IntStream.rangeClosed(0 ,100).forEach(i->{
            int x = (int)Math.floor(Math.random()*100);
            if (x%2== 0) {
                div2.add(x);
            } else if (x%3 ==0) {
                div3.add(x);
            } else other.add(x);
        });

        System.out.println(div2);
        System.out.println("====================");
        System.out.println(div3);
        System.out.println("====================");
        while (other.size()>0) {
            System.out.print(other.poll()+", ");
        }

    }

    public static void main(String[] args) {
        List<Integer> numbers =
                IntStream.rangeClosed(0, 100)
                        .boxed()
                        .map(x->(int)Math.floor(Math.random()*100))
                        .collect(Collectors.toList());

        String div2 = numbers.stream()
                .filter(x -> x % 2 == 0)
                .sorted(new Comparator<Integer>() {
                    @Override
                    public int compare(Integer o1, Integer o2) {
                        return o1 - o2;
                    }
                })
                .map(integer -> String.format("<%d>", integer))
                .collect(Collectors.joining(":"));

        Set<Integer> div3 =
                numbers.stream()
                        .filter(x -> x % 3 == 0)
                        .collect(Collectors.toSet());

        PriorityQueue<Integer> other =
                numbers.stream()
                        .filter(x -> (x % 2 != 0) & (x % 3 != 0))
                        .collect(Collectors.toCollection(PriorityQueue::new));

        System.out.println(div2);
        System.out.println("=============================");
        System.out.println(div3);
        System.out.println("=============================");
        while (other.size()>0) {
            System.out.print(other.poll()+", ");
        }
    }

}
