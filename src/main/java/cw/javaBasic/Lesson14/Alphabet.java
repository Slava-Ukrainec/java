package cw.javaBasic.Lesson14;

import java.util.stream.IntStream;

public class Alphabet {
    public static void main(String[] args) {
        IntStream.rangeClosed('A','Z')
                .forEach(x-> System.out.println((char)x));
    }
}
