package cw.javaBasic.Lesson14;

import cw.javaBasic.Lesson7.lettersCounter.Pair;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class IntStreamEx {
    public static void main(String[] args) {
        String s = "Hello World";

        IntStream.range(0, s.length())
                .mapToObj(i-> new Pair<Character, Integer>(s.charAt(i),i))
                .collect(Collectors.toList());

    }
}
