package cw.javaBasic.Lesson14;

import java.util.ArrayList;
import java.util.Optional;

public class Optnl {
    public static void main(String[] args) {
        ArrayList<Optional<String>> a = new ArrayList<>();

        Optional<String> extra1 = Optional.of("info");
        Optional<String> extra2 = Optional.empty();

        a.add(extra1);
        a.add(extra2);

        // First example
        a.forEach(os->os.ifPresent(System.out::println));
        a.forEach(os->os.ifPresent(System.out::println));


    }
}
