package cw.javaBasic.Lesson14;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamEx001 {
    static int[] data() {return new int[]{1,2,3,4,5,6,7,8,9};}

    public static void main1(String[] args) {
        int[] ints = {1,2,3,4,5,6,7,8,9};
        ArrayList<Integer> result1 = new ArrayList<>();
        for (int i = 0; i < ints.length; i++) {
            if(ints[i]%2 ==0) {
                result1.add(ints[i]);
            }
            System.out.println(result1);
        }

        ArrayList<Integer> result2 = new ArrayList<>();
        for (int i = 0; i < ints.length; i++) {
            if(ints[i] > 5) {
                result1.add(ints[i]);
            }
            System.out.println(result2);
        }
    }

    public static void main(String[] args) {
        IntStream stream1 = Arrays.stream(data());
        IntStream stream2 = stream1.filter(x-> x>5);
        Stream<Integer> stream3 = stream2.boxed();
        Stream<Integer> stream4 = stream3.map(x -> x * 2);
        Stream<Integer> stream5 = stream4.map(x->x+1);
        List<Integer> collected = stream5.collect(Collectors.toList());
        System.out.println(collected);

    }
}
