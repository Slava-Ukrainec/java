package cw.javaEE.lesson5.practice.servlets;

import cw.javaEE.lesson5.practice.CookieMgr;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;


public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        CookieMgr cookieMgr = new CookieMgr(req.getCookies());
        if (cookieMgr.getCookieValue("user")==null) {
            Files.copy(Paths.get("./assets", "/login.html"), resp.getOutputStream());
        } else {
            resp.sendRedirect("/calc/");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String user = req.getParameter("login");
        Cookie cookie = new Cookie("user",user);
        cookie.setMaxAge(3600);
        cookie.setPath("/");
        resp.addCookie(cookie);
        resp.sendRedirect("/calc/");
    }
}
