package cw.javaEE.lesson5.practice.servlets;

import cw.javaEE.lesson5.practice.CalcHistory;
import cw.javaEE.lesson5.practice.CookieMgr;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

public class HistoryServlet extends HttpServlet {
   CalcHistory history;

    public HistoryServlet(CalcHistory history) {
        this.history = history;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        CookieMgr cookieMgr = new CookieMgr(req.getCookies());
        String user = cookieMgr.getCookieValue("user");
        if (user == null) user = "unregistered";

        PrintWriter writer = resp.getWriter();


        writer.println("<html><body>");
        writer.println(user + " operations");
        writer.println("<br>");
        try {
            history.get(user).forEach(writer::println);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("ERROR WHILE GETTING FROM SQL");
        }
        writer.println("<br>");
        writer.println("<a href=\"/calc/\"><button>other calculation</button></a>");
        writer.println("<a href=\"/logout\"><button>log out</button></a>");
        writer.println("</body></html>");
    }
}
