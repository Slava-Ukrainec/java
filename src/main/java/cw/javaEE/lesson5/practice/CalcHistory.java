package cw.javaEE.lesson5.practice;

import cw.javaEE.lesson1.practice.Operation;
import cw.javaEE.lesson5.DbConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class CalcHistory {
    private Connection connection = DbConnection.getConnection();

    public void put (String user, Opp op) throws SQLException {
        String SQL_I = "INSERT INTO calc_operations (\"id\", \"x\", \"op\", \"y\", \"user\") VALUES (DEFAULT, ?, ?, ?, ?)";
        // statement
        PreparedStatement stmti = connection.prepareStatement(SQL_I);
        // setting data
        stmti.setInt(1, op.getX());
        stmti.setString(2, op.getOp());
        stmti.setInt(3, op.getY());
        stmti.setString(4, user);
        // running
        stmti.execute();
    }

//    public void put (String user, Opp op) {
//        List<Opp> items = storage.computeIfAbsent(user, k -> new ArrayList<>());
//        items.add(op);
//    }

    public List get (String user) throws SQLException {

        // select sql
        String SQL_S = "SELECT \"x\", \"op\", \"y\" FROM calc_operations WHERE \"user\" = ?";
        // statement
        PreparedStatement stmt = connection.prepareStatement(SQL_S);

        stmt.setString(1,user);
        // running
        ResultSet rset = stmt.executeQuery();
        // processing data

        List<Opp> result = new ArrayList<>();
        String x, y;
        while (rset.next()) {
            x = String.valueOf(rset.getInt("x"));
            y = String.valueOf(rset.getInt("y"));
            result.add(Opp.build(rset.getString("op"),x,y));
        }

        return result;
    }
}
