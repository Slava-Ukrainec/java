package cw.javaEE.lesson2.practice;

import cw.javaEE.lesson2.practice.servlets.*;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;


public class MyApp {
    public static void main(String[] args) throws Exception {
        CalcHistory history = new CalcHistory();
        Server server = new Server(8000);

        ServletContextHandler handler = new ServletContextHandler();
        handler.addServlet(new ServletHolder(new HistoryServlet(history)), "/history");
        handler.addServlet(new ServletHolder(new OperationServlet(history)), "/calc/*");
        handler.addServlet(new ServletHolder(new LoginServlet()), "/login/*");
        handler.addServlet(new ServletHolder(new LogoutServlet()), "/logout/*");
        handler.addServlet(new ServletHolder(new ClearCookiesServlet()), "/clearCookies");
        handler.addServlet(new ServletHolder(new ServletRedirectTo("/login/")), "/*");


        server.setHandler(handler);

        server.start();
        server.join();
    }
}


