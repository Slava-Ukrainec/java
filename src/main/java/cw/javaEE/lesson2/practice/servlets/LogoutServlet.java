package cw.javaEE.lesson2.practice.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


public class LogoutServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie[] cookies = req.getCookies();

        if(cookies == null){
           throw new IllegalArgumentException("User hasn't been authorized before");
        }

        for (Cookie c : cookies){
            if (c.getName().equals("user")) {
                c.setMaxAge(0);
                resp.addCookie(c);
                resp.sendRedirect("/login");
            }
        }
    }
}
