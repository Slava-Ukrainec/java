package cw.javaEE.lesson2.practice.servlets;

import cw.javaEE.lesson2.practice.CookieMgr;
import org.eclipse.jetty.servlet.Source;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.print.Book;
import java.io.IOException;
import java.io.PrintWriter;

public class ClearCookiesServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter writer = resp.getWriter();
        if (CookieMgr.clearAllCookies(req, resp)) {
            writer.println("All cookies have been cleared");
        } else writer.println("There were no cookies");
    }
}
