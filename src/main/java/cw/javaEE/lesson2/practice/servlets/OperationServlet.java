package cw.javaEE.lesson2.practice.servlets;

import cw.javaEE.lesson2.practice.CalcHistory;
import cw.javaEE.lesson2.practice.CookieMgr;
import cw.javaEE.lesson2.practice.Opp;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;


public class OperationServlet extends HttpServlet {

    private CalcHistory history;

    public OperationServlet(CalcHistory history) {
        this.history = history;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        CookieMgr cookieMgr = new CookieMgr(req.getCookies());
        if (cookieMgr.getCookieValue("user")!=null) {
            Files.copy(Paths.get("./assets", "/calculator.html"), resp.getOutputStream());
        } else {
            resp.sendRedirect("/login");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String x = req.getParameter("x");
        String y = req.getParameter("y");
        String operator = req.getParameter("operator");

        CookieMgr cookieMgr = new CookieMgr(req.getCookies());
        String user = cookieMgr.getCookieValue("user");
        if (user == null) user = "unregistered";

//        String pathInfo = req.getPathInfo();
        Opp op = Opp.build(operator,x,y);
        PrintWriter writer = resp.getWriter();

        if (op != null) {
            history.put(user, op);
            writer.println("<html><body>");
            writer.println(op);
            writer.println("<br>");
            writer.println("<a href=\"/calc/\"><button>other calculation</button></a>");
            writer.println("<a href=\"/history\"><button>history</button></a>");
            writer.println("</body></html>");
        } else {
            writer.println("Invalid operator" + operator);
        }
    }
}
