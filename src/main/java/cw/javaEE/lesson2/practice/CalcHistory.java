package cw.javaEE.lesson2.practice;

import java.util.*;

public class CalcHistory {
    private final Map<String, List<Opp>> storage = new HashMap<>();
    private final List EMPTY = Collections.unmodifiableList(Collections.emptyList());

    public void put (String user, Opp op) {
        List<Opp> items = storage.computeIfAbsent(user, k -> new ArrayList<>());
        items.add(op);
    }

    public List get (String user) {
        return storage.getOrDefault(user, EMPTY);
    }
}
