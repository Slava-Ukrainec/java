package cw.javaEE.lesson2.practice;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CookieMgr {
    private final Cookie[] cookies;

    public CookieMgr(Cookie[] cookies) {
        this.cookies = cookies;
    }

    public String getCookieValue(String Cname){
        if (cookies != null) {
            for (Cookie c : cookies) {
                if (c.getName().equals(Cname)) {
                    return c.getValue();
                }
            } return null;
        } else return null;
    }

    public static boolean clearAllCookies(HttpServletRequest req, HttpServletResponse resp) {
        Cookie[] cookies = req.getCookies();

        if(cookies == null) return false;

        for (Cookie c : cookies){
            c.setMaxAge(0);
            resp.addCookie(c);
        }
        return true;
    }
}
