package cw.javaEE.lesson2.practice;

public interface Opp {
    int x = 0;
    int y = 0;

    static Opp build(String operation, String x, String y){
        int a = Integer.parseInt(x);
        int b = Integer.parseInt(y);
        switch (operation){
            case "/add":
                return new Add(a,b);
            case "/div":
                return new Div(a,b);
            case "/mul":
                return new Mul(a,b);
            case "/sub":
                return new Sub(a,b);
            default:
                return null;
        }
    }

    default int getX() {
        return x;
    }

    default int getY() {
        return y;
    }

    int getResult();

    class Add implements Opp {
        private final int x, y;

        public Add(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public int getResult() {
            return x + y ;
        }

        @Override
        public String toString() {
            return String.format("ADD OPERATION: %s + %s = %s", x, y, getResult());
        }
    }

    class Sub implements Opp {
        private final int x, y;

        public Sub(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public int getResult() {
            return x - y ;
        }

        @Override
        public String toString() {
            return String.format("SUB OPERATION: %s - %s = %s", x, y, getResult());
        }
    }

    class Div implements Opp {
        private final int x, y;

        public Div(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public int getResult() {
            return x / y ;
        }

        @Override
        public String toString() {
            return String.format("DIV OPERATION: %s / %s = %s", x, y, getResult());
        }
    }

    class Mul implements Opp {
        private final int x, y;

        public Mul(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public int getResult() {
            return x * y ;
        }

        @Override
        public String toString() {
            return String.format("MUL OPERATION: %s * %s = %s", x, y, getResult());
        }
    }
}
