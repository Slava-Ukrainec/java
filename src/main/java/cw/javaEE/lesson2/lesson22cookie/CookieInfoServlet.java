package cw.javaEE.lesson2.lesson22cookie;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class CookieInfoServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter w = resp.getWriter();
        w.println("Format is:");
        w.println("var=NAME");
        w.println("val=VALUE");
        w.println("time=TIMEOUT(sec)");
        w.println("http://localhost:8002          - this page");
        w.println("http://localhost:8002/c/list   - list all the cookies");
        w.println("http://localhost:8002/c/get    - get one cookie (by name)");
        w.println("http://localhost:8002/c/set    - set one cookie (name, val, time)");
        w.println("http://localhost:8002/c/del    - delete one cookie (by name)");
        w.println("http://localhost:8002/c/delall - delete all cookie (no params)");
    }
}
