package cw.javaEE.lesson2.lesson22cookie;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class WebApp03 {
    public static void main(String[] args) throws Exception {
        Server server = new Server(8002);
        ServletContextHandler handler = new ServletContextHandler();
        handler.addServlet(new ServletHolder(new CookieListServlet()), "/c/list/*");
        handler.addServlet(new ServletHolder(new CookieDelServlet()), "/c/del/*");
        handler.addServlet(new ServletHolder(new CookieGetServlet()), "/c/get/*");
        handler.addServlet(new ServletHolder(new CookieSetServlet()), "/c/set/*");
        handler.addServlet(new ServletHolder(new CookieDeleteAllServlet()), "/c/delall");
        handler.addServlet(new ServletHolder(new CookieInfoServlet()), "/*");

        server.setHandler(handler);
        server.start();
        server.join();
    }
}
