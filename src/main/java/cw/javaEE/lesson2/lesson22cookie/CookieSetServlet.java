package cw.javaEE.lesson2.lesson22cookie;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class CookieSetServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String name = req.getParameter("name");
        String val = req.getParameter("val");
        int time = Integer.valueOf(req.getParameter("time"));
        Cookie cookie = new Cookie(name, val);
        cookie.setMaxAge(time);
        resp.addCookie(cookie);
        PrintWriter w = resp.getWriter();
        w.println(CookiePrinter.toString(cookie) + "has been set");
    }
}
