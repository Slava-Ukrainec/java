package cw.javaEE.lesson2.lesson22cookie;

import javax.servlet.http.Cookie;

public class CookiePrinter {
    static String toString(Cookie cookie) {
        return String.format("Cookie:%s, value:%s, time:%d(sec)",
                cookie.getName(), cookie.getValue(), cookie.getMaxAge());
    }
}
