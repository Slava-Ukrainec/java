package cw.javaEE.lesson6.builderpattern;

public class BigBuilder {
    int age;
    int sex;
    String name;
    String parent1;
    String parent2;

    BigBuilder withAge(int age) {
        this.age = age;
        return this;
    };

    BigBuilder withSex(int sex) {
        this.sex = sex;
        return this;
    };

    BigBuilder withName (String name) {
        this.name = name;
        return this;
    };

    BigBuilder withParent1 (String parent) {
        this.parent1 = name;
        return this;
    };

    BigBuilder withParent2 (String parent) {
        this.parent2 = parent;
        return this;
    };

    BigObj build(){
        BigObj obj = new BigObj();
        if (age != 0) {
            obj.setAge(age);
        }
        if (name != null) {
            obj.setName(name);
        }
        if (sex != 0) {
            obj.setSex(sex);
        }
        if (parent1 != null) {
            obj.setParent1(parent1);
        }
        if (parent2 != null) {
            obj.setParent2(parent2);
        }

        return obj;
    }
}
