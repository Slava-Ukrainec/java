package cw.javaEE.lesson6.builderpattern;

public class BigObj {
    int age;
    int sex;
    String name;
    String parent1;
    String parent2;

    public BigObj() {}

    public void setAge(int age) {
        this.age = age;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setParent1(String parent1) {
        this.parent1 = parent1;
    }

    public void setParent2(String parent2) {
        this.parent2 = parent2;
    }
}
