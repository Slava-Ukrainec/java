package cw.javaEE.lesson6.decoratorpattern;

import cw.javaBasic.Lesson8.Interface.Thinkable;

public class ThinkThreeTimes implements Thinkable3 {
    private Thinkable3 origin;

    public ThinkThreeTimes (Thinkable3 origin) {
        this.origin = origin;
    }

    @Override
    public void think() {
        for (int i = 0; i < 3 ; i++) {
            origin.think();
        }
    }
}
