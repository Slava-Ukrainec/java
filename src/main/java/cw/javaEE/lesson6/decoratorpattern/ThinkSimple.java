package cw.javaEE.lesson6.decoratorpattern;

public class ThinkSimple implements Thinkable3 {

    @Override
    public void think() {
        System.out.println("I am thinking");
    }
}
