package cw.javaEE.lesson6.decoratorpattern;

import cw.javaBasic.Lesson8.Interface.Thinkable;

public class App {
    public static void main(String[] args) {
        Thinkable3 th = new ThinkSimple();

        Thinkable3 th2 = new ThinkThreeTimes(th);
        th2.think();

        Thinkable3 wHeader = new ThinkWithHeader(th2);
        wHeader.think();
    }
}
