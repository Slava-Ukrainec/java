package cw.javaEE.lesson6.decoratorpattern;

public class ThinkWithHeader implements Thinkable3 {
    private final Thinkable3 origin;

    public ThinkWithHeader(Thinkable3 origin) {
        this.origin = origin;
    }


    @Override
    public void think() {
        System.out.println("===========HEADER============");
        origin.think();
        System.out.println("===========FOOTER============");
    }
}
