package cw.javaEE.lesson6.geometryTask;

public class App {
    public static void main(String[] args) {
        AreaCalculator ac = new AreaCalculator();

        ac.addFigure(new Point(0,0), new Point(4,2));
        ac.addFigure(new Point(3,1), new Point(5,3));
        int result = ac.calcArea();
        System.out.println(result);
    }
}
