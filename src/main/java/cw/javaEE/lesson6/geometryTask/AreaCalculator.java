package cw.javaEE.lesson6.geometryTask;

import java.util.HashSet;
import java.util.Set;

public class AreaCalculator {
    private final Set<Point> storage = new HashSet();

    public void addFigure(Point one, Point two){
        for (int i = one.getX(); i < two.getX() ; i++) {
            for (int j = one.getY(); j < two.getY() ; j++) {
                storage.add(new Point(i,j));
            }
        }
    };

    public int calcArea() {
        return storage.size();
    }
}
