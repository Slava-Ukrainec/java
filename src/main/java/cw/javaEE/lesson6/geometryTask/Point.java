package cw.javaEE.lesson6.geometryTask;

public class Point {
    private final int x;
    private final int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public boolean equals(Object another) {
        if (this == another) return true;
        if (another == null) return false;
        if (this.getClass() != another.getClass()) return false;
        Point p = (Point) another;
        return (x == p.getX() & y == p.getY());
    }

    @Override
    public int hashCode(){
        return x * 17 + y;
    }
}
