package cw.javaEE.lesson6.facadepattern;

import cw.javaEE.lesson6.facadepattern.was.Class1;

public class Algo1 implements Printable42 {
    private final Class1 origin = new Class1();

    @Override
    public void print() {
        origin.print(7);
    }
}
