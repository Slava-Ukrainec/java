package cw.javaEE.lesson6.facadepattern;

public class FacadeApp {
    public static void main(String[] args) {
        Printable42 a1 = new Algo1();
        Printable42 a2 = new Algo2();

        a1.print();
        a2.print();
    }
}
