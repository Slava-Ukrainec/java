package cw.javaEE.lesson6.facadepattern;

import cw.javaEE.lesson6.facadepattern.was.Library2;

public class Algo2 implements Printable42 {
    private final Library2 origin = new Library2();
    @Override
    public void print() {
        origin.iamthesmartestpersonever(7);
    }
}
