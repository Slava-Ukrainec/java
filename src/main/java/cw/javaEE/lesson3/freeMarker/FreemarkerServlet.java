package cw.javaEE.lesson3.freeMarker;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class FreemarkerServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        FreeMarker fm = new FreeMarker("./templates");
        HashMap<String, Object> data = new HashMap<>();
        data.put("name", "Vasya");
        data.put("fruits", new ArrayList<String>() {{
            add("Apple");
            add("Orange");
            add("Banana");
        }});
        fm.render("example.ftl", data, resp);
    }
}
