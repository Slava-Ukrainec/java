package cw.javaEE.lesson3.freeMarker;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;


public class MyApp {
    public static void main(String[] args) throws Exception {
        Server server = new Server(8000);

        ServletContextHandler handler = new ServletContextHandler();
        handler.addServlet(new ServletHolder(new ServletAssets()), "/assets");
        handler.addServlet(new ServletHolder(new FreemarkerServlet()), "/freemarker");


        server.setHandler(handler);

        server.start();
        server.join();
    }
}


