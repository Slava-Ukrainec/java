package cw.javaEE.lesson3.grizzly;

import cw.javaEE.lesson2.practice.Opp;
import cw.javaEE.lesson3.freeMarker.FreeMarker;
import cw.javaEE.lesson3.grizzly.CookieMgr;
import org.eclipse.jetty.server.Request;
import org.glassfish.grizzly.http.HttpRequestPacket;
import org.glassfish.grizzly.http.server.SessionManager;

import javax.servlet.SessionCookieConfig;
import javax.ws.rs.*;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;

@Path("/calc")
public class MyResource {

    @GET
    public String calc_get() throws IOException {
        FreeMarker template = new FreeMarker("./templates");
        StringWriter sw = new StringWriter();
        template.render("calculator.ftl", sw);
        return sw.toString();
    }

    @POST
    public String calc_post(MultivaluedMap<String, String> formParams) throws IOException {
        FreeMarker template = new FreeMarker("./templates");
        StringWriter sw = new StringWriter();
        String x = formParams.getFirst("x");
        String y = formParams.getFirst("y");
        String operator = formParams.getFirst("operator");

        Opp op = Opp.build(operator,x,y);
        HashMap<String, Object> data = new HashMap<>();

        if (op != null) {
            data.put("op", op.toString());
            template.render("calcresult.ftl", data, sw);
            return sw.toString();
        } else return "Invalid input";
    }

    @Path("/login")
    @GET
    public String login_get() throws IOException {
        FreeMarker template = new FreeMarker("./templates");
        StringWriter sw = new StringWriter();
        template.render("login.ftl", sw);
        return sw.toString();
    }

    @Path("/login")
    @POST
    public String login_post(MultivaluedMap<String, String> formParams) throws IOException {
        String user = formParams.getFirst("user");
        FreeMarker template = new FreeMarker("./templates");
        StringWriter sw = new StringWriter();
        template.render("login.ftl", sw);
        return sw.toString();
    }

    @Path("/logout")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String logout() {
        return "This is logout page";
    }

    @Path("/history")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String history() {
        return "This is history page";
    }


}
