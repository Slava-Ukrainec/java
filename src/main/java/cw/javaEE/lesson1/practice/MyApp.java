package cw.javaEE.lesson1.practice;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import java.util.ArrayList;


public class MyApp {
    public static void main(String[] args) throws Exception {
        ArrayList<Operation> history = new ArrayList<>();
        Server server = new Server(8000);

        ServletContextHandler handler = new ServletContextHandler();

        handler.addServlet(new ServletHolder(new HistoryServlet(history)), "/history");
        handler.addServlet(new ServletHolder(new OperationServlet(history)), "/*");


        server.setHandler(handler);

        server.start();
        server.join();
    }
}


