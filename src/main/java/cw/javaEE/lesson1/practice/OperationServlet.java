package cw.javaEE.lesson1.practice;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class OperationServlet extends HttpServlet {

    private ArrayList<Operation> history;

    public OperationServlet(ArrayList<Operation> history) {
        this.history = history;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String x = req.getParameter("x");
        String y = req.getParameter("y");
        String pathInfo = req.getPathInfo();
        PrintWriter writer = resp.getWriter();

        Map<String, Operations> scenario = new HashMap<>();
        scenario.put("/add", Operations.ADD);
        scenario.put("/sub", Operations.SUB);
        scenario.put("/div", Operations.DIV);
        scenario.put("/mul", Operations.MUL);

        Operations op = scenario.get(pathInfo);

        if (op != null) {
            writer.println(op.name());
            Operation operation = new Operation(op,x,y);
            history.add(operation);
            writer.println(operation);
        }
    }
}
