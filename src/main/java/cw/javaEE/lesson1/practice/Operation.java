package cw.javaEE.lesson1.practice;

public class Operation {
    private final Operations operation;
    private final int x;
    private final int y;

    Operation( Operations operation, String x, String y){
        this.operation = operation;
        this.x = Integer.parseInt(x);
        this.y = Integer.parseInt(y);
    }

    public int getResult(){
        switch (operation){
            case ADD:
                return this.x + this.y;
            case DIV:
                return this.x / this.y;
            case MUL:
                return this.x * this.y;
            case SUB:
                return this.x - this.y;
            default:
                return 0;
        }
    }
    public Operations getOperation() {
        return operation;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public String toString(){
        return String.format("%s OPERATION: %s %s %s = %s", operation.name(), x, operation.op(), y, getResult());

    }
}
