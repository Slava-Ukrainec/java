package cw.javaEE.lesson1.practice;

public enum Operations {
    ADD(" + "),
    SUB(" - "),
    DIV(" / "),
    MUL(" * ");

    private final String op;

    Operations(String op){
        this.op = op;
    }

    public String op() {
        return op;
    }
}
