package cw.javaEE.lesson1;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CustomWebApp {
    public static void main(String[] args) throws Exception {

        Server server = new Server(8000);

        ServletContextHandler handler = new ServletContextHandler();

        handler.addServlet(new ServletHolder(new HttpServlet() {
            @Override
            protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
                resp.getWriter().write("hello world");
            }
        }),"/*");

        server.setHandler(handler);

        server.start();
        server.join();
    }
}
