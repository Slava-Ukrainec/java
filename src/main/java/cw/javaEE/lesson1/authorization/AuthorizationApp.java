package cw.javaEE.lesson1.authorization;

import cw.javaEE.lesson1.authorization.servlets.AuthorizationServlet;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import java.util.HashMap;
import java.util.Map;


public class AuthorizationApp {
    private final Map<String,String> authorization = new HashMap<>();
    public static void main(String[] args) throws Exception {
        Server server = new Server(8000);

        ServletContextHandler handler = new ServletContextHandler();

        handler.addServlet(new ServletHolder(new AuthorizationServlet()), "/auth");


        server.setHandler(handler);

        server.start();
        server.join();
    }

    public static void createData(String[] args) {

    }
}
